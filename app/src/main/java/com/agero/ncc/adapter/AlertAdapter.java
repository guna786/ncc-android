package com.agero.ncc.adapter;


import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.agero.ncc.R;
import com.agero.ncc.db.entity.Alert;
import com.agero.ncc.utils.DateTimeUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Adapter for AlertFragment
 */

public class AlertAdapter extends RecyclerView.Adapter<AlertAdapter.ItemViewHolder> {


    private Context mContext;
    private List<Alert> mAlertItem;


    public AlertAdapter(Context mContext, List<Alert> mAlertItem) {
        this.mContext = mContext;
        this.mAlertItem = mAlertItem;


    }


    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ItemViewHolder(LayoutInflater.from(mContext).inflate(R.layout.alert_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {

        if(mAlertItem.get(holder.getAdapterPosition()).getAlertTitle() == null){
            mAlertItem.get(holder.getAdapterPosition()).setAlertTitle("");
        }

        if (mAlertItem.get(holder.getAdapterPosition()).getAlertTitle().startsWith("Today")
                || mAlertItem.get(holder.getAdapterPosition()).getAlertTitle().equalsIgnoreCase("Old")) {
            holder.mTextAlertSection.setText(mAlertItem.get(holder.getAdapterPosition()).getAlertTitle());
            holder.mTextAlertSection.setVisibility(View.VISIBLE);
            holder.mConstraintAlerts.setVisibility(View.GONE);
            holder.mImageAlertClose.setVisibility(View.VISIBLE);
        } else {
            holder.mTextAlertSection.setVisibility(View.GONE);
            holder.mConstraintAlerts.setVisibility(View.VISIBLE);
            holder.mImageAlertClose.setVisibility(View.GONE);
            holder.mTextAlertService.setText(mAlertItem.get(position).getAlertTitle());
           /* if (mAlertItem.get(position).getAlertDescription().length() > 35) {
                String mConcatenateText = mAlertItem.get(position).getAlertDescription().substring(0, 34) + "...";
                holder.mTextAlertReason.setText(mConcatenateText);
            } else {*/
                holder.mTextAlertReason.setText(mAlertItem.get(position).getAlertDescription());
//            }
            holder.mTextAlertJobEta.setText(DateTimeUtils.getDisplayTimeStamp(mAlertItem.get(position).getUpdatedTime()));

            if(mAlertItem.get(holder.getAdapterPosition()) != null && mAlertItem.get(holder.getAdapterPosition()).getRead() != null && mAlertItem.get(holder.getAdapterPosition()).getRead()){
                holder.mImageAlertStatus.setVisibility(View.GONE);
            }else{
                holder.mImageAlertStatus.setVisibility(View.VISIBLE);
            }
        }

        if (mAlertItem.size() - 1 == position) {
            holder.mViewAlertBorder.setVisibility(View.INVISIBLE);
        } else {
            holder.mViewAlertBorder.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return mAlertItem.size();
    }

    /**
     * To get imageId based on status type
     *
     * @param type
     * @return imageId
     */
    private int getImageResourceByType(String type) {
        int imageId = 0;
        switch (type) {
            case "NEW":
                imageId = R.drawable.ic_briefcase_greencircle;
                break;
            case "ASSIGNED":
                imageId = R.drawable.ic_alert_yellowcircle;
                break;
            case "CANCELLED":
                imageId = R.drawable.ic_alert_cancel_redcircle;
        }
        return imageId;
    }

    /**
     * View Holder for Alert list.
     */
    static class ItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image_alert_status)
        ImageView mImageAlertStatus;
        @BindView(R.id.text_alert_service)
        TextView mTextAlertService;
        @BindView(R.id.text_alert_reason)
        TextView mTextAlertReason;
        @BindView(R.id.text_alert_job_eta)
        TextView mTextAlertJobEta;
        @BindView(R.id.view_alert_border)
        View mViewAlertBorder;
        @BindView(R.id.constraint_alerts)
        ConstraintLayout mConstraintAlerts;
        @BindView(R.id.text_alert_section)
        TextView mTextAlertSection;
        @BindView(R.id.image_alert_close)
        ImageView mImageAlertClose;

        ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
