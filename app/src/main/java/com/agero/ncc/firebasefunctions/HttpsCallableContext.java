package com.agero.ncc.firebasefunctions;

import android.support.annotation.Nullable;

class HttpsCallableContext {
    @Nullable
    private final String authToken;
    @Nullable
    private final String instanceIdToken;

    HttpsCallableContext(@Nullable String authToken, @Nullable String instanceIdToken) {
        this.authToken = authToken;
        this.instanceIdToken = instanceIdToken;
    }

    @Nullable
    public String getAuthToken() {
        return this.authToken;
    }

    @Nullable
    public String getInstanceIdToken() {
        return this.instanceIdToken;
    }
}

