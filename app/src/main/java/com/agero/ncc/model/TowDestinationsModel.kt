package com.agero.ncc.model


data class TowDestinationsRequest(
        var vehicleMake: String? = null,
        var vehicleModel: String? = null,
        var callType: Int? = null,
        var vehicleYear: String? = null,
        var disablementReason: String? = null,
        var state: String? = null,
        var postalCode: String? = null,
        var latitude: String? = null,
        var longitude: String? = null,
        var additionalParameters: AdditionalParameters? = null
)

data class AdditionalParameters(
        var clientCode: String? = null,
        var isEmployee: String? = null,
        var billGroup: String? = null,
        var towEnabled: String? = null,
        var nonCoverage: String? = null,
        var sellingDealerId: String? = null
)

data class TowDestinationsModel(
        var towDestinations: List<TowDestinationEntity>? = null
)

data class TowDestinationEntity(
        var vendorId: String? = null,
        var facilityId: String? = null,
        var vendorName: String? = null,
        var address: String? = null,
        var city: String? = null,
        var state: String? = null,
        var postalCode: String? = null,
        var latitude: Double? = null,
        var longitude: Double? = null,
        var characteristic: String? = null,
        var hoursOfOperationWeekday: String? = null,
        var hoursOfOperationSaturday: String? = null,
        var hoursOfOperationSunday: String? = null,
        var nightDropOff: String? = null,
        var callAhead: String? = null,
        var score: Double? = null,
        var order: Double? = null,
        var crowFlyDistance: Double? = null,
        var drivingDistance: Double? = null
)