package com.agero.ncc.model


data class AlertResponse(
        var totalCount: Long? = null, // 3
        var results: List<AlertResult>? = null
)

data class AlertResult(
        var jobId: String? = null, // #1233
        var updatedTime: String? = null, // 2016-09-13T23:30:52.123Z
        var status: AlertStatus? = null,
        var title: String? = null, // New Job Assignment
        var reason: String? = null,
        var description: String? = null// You have been assigned a Tow
)

data class AlertStatus(
        var id: Long? = null,
        var type: String? = null
)


