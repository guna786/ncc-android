package com.agero.ncc.utils;

import android.graphics.Color;
import android.os.AsyncTask;
import android.text.TextUtils;

import com.agero.ncc.R;
import com.agero.ncc.activities.BaseActivity;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Callable;

import org.json.JSONException;
import org.json.JSONObject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class DownloadDirections {

  private DirectionDownloadedListener mDirectionDownloadedListener;

  public DownloadDirections(DirectionDownloadedListener directionDownloadedListener) {
    mDirectionDownloadedListener = directionDownloadedListener;
  }

  public Disposable execute(String... jsonData){
      Observable<List<List<HashMap<String, String>>>> observable = Observable.fromCallable(new Callable<List<List<HashMap<String, String>>>>() {
          @Override
          public List<List<HashMap<String, String>>> call() throws Exception {
              return doInBackground(jsonData);
          }
      });

    return observable.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe((result) -> {
          onPostExecute(result);
      },Throwable::printStackTrace);
  }

  private List<List<HashMap<String, String>>> doInBackground(String... jsonData) {
    String directionsResponse = null;
    try {
      directionsResponse = Utils.getDirectionsResponse(jsonData[0]);
    } catch (IOException e) {
      e.printStackTrace();
    }
    JSONObject jObject;
    List<List<HashMap<String, String>>> routes = null;

    try {
      if(!TextUtils.isEmpty(directionsResponse)) {
        jObject = new JSONObject(directionsResponse);

        DirectionsJSONParser parser = new DirectionsJSONParser();

        routes = parser.parse(jObject);
      }

    } catch (JSONException je){
      je.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return routes == null ? new ArrayList<>() : routes;
  }

  private void onPostExecute(List<List<HashMap<String, String>>> result) {
    ArrayList<LatLng> points;
    PolylineOptions polylineOptions = new PolylineOptions();
    if(result != null) {
      for (int i = 0; i < result.size(); i++) {
        points = new ArrayList<>();
        List<HashMap<String, String>> path = result.get(i);

        for (int j = 0; j < path.size(); j++) {
          HashMap<String, String> point = path.get(j);

          double lat = Double.parseDouble(point.get("lat"));
          double lng = Double.parseDouble(point.get("lng"));
          LatLng position = new LatLng(lat, lng);

          points.add(position);
        }

        polylineOptions.addAll(points);
        polylineOptions.width(8);
        polylineOptions.color(Color.BLACK);
        polylineOptions.geodesic(true);
      }
    }
    if (polylineOptions != null) {
      mDirectionDownloadedListener.onSuccess(polylineOptions);
    } else {
      mDirectionDownloadedListener.onFailure();
    }
  }

   public interface DirectionDownloadedListener{
    void onSuccess(PolylineOptions polylineOptions);
    void onFailure();
  }
}
