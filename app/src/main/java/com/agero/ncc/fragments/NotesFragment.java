package com.agero.ncc.fragments;

import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.agero.ncc.R;
import com.agero.ncc.activities.BaseActivity;
import com.agero.ncc.activities.HomeActivity;
import com.agero.ncc.adapter.NotesListAdapter;
import com.agero.ncc.model.Comment;
import com.agero.ncc.model.ExtenuationCircumstancesItem;
import com.agero.ncc.model.JobDetail;
import com.agero.ncc.utils.DateTimeUtils;
import com.agero.ncc.utils.NccConstants;
import com.agero.ncc.utils.TokenManager;
import com.agero.ncc.utils.UiUtils;
import com.agero.ncc.utils.UserError;
import com.agero.ncc.utils.Utils;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.nikhilpanju.recyclerviewenhanced.RecyclerTouchListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import durdinapps.rxfirebase2.RxFirebaseDatabase;

import static com.agero.nccsdk.NccSdk.getApplicationContext;


public class NotesFragment extends BaseFragment {

    HomeActivity mHomeActivity;
    @BindView(R.id.text_notes_desc)
    TextView mTextNotesDesc;
    @BindView(R.id.recycler_notes)
    RecyclerView mRecyclerNotes;
    Unbinder unbinder;
    @BindView(R.id.edit_notes)
    EditText mEditNotes;
    @BindView(R.id.text_add)
    TextView mTextAdd;
    private RecyclerTouchListener onTouchListener;
    ArrayList<Comment> mNotesList, mExtenuationCircumstance;
    NotesListAdapter mNotesListAdapter;
    Comment notes;
    @BindView(R.id.view)
    View mView;
    private long oldRetryTime = 0;
    private UserError mUserError;
    DatabaseReference myRef;
    private String mDispatchId;
    private String mDispatchCreatedDate;
    View view;
    boolean isCircumstanceCheck;
    ArrayList<Comment> mCommentListAll;
    boolean isEditable;

    private String mTimezone;
    private JobDetail mCurrentJob;

    ValueEventListener valueEventListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            if (isAdded()) {
                hideProgress();
                if (dataSnapshot != null && dataSnapshot.getValue(JobDetail.class) != null) {
                    loadNotesList(dataSnapshot.getValue(JobDetail.class));
                }
            }
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            hideProgress();
            if (isAdded()) {
                if (databaseError != null && databaseError.getMessage() != null) {
                    mHomeActivity.mintlogEvent("Notes Comments List Firebase DatabaseError - Error code - " + databaseError.getCode() + " Error Message -" + databaseError.getMessage());
                }
                if (oldRetryTime == 0 || (System.currentTimeMillis() - oldRetryTime) > NccConstants.RETRY_LIMIT_MILLI_SEC) {
                    getNotesFromFirebase();
                    oldRetryTime = System.currentTimeMillis();
                }
            }
        }
    };

    private void loadNotesList(JobDetail jobDetail) {
        ButterKnife.bind(this, view);
        mNotesList = new ArrayList<>();
        if (mExtenuationCircumstance == null) {
            mExtenuationCircumstance = new ArrayList<>();
        }
        mCommentListAll = new ArrayList<>();
//        if (dataSnapshot.hasChildren()) {
//            Iterable<DataSnapshot> childList = dataSnapshot.getChildren();
//            for (DataSnapshot data : childList) {
//                Comment comment = data.getValue(Comment.class);
//                this.mNotesList.add(comment);
//            }
//        }
        mNotesList.clear();
        try {

            if (jobDetail.getComments() != null) {
                List<Comment> comments = jobDetail.getComments();
                mTimezone = jobDetail.getDisablementLocation().getTimeZone();
                for (Comment comment : comments) {
                    this.mNotesList.add(comment);
                }
            }

            HashMap<String, String> extraDatas = new HashMap<>();
            extraDatas.put("dispatch_id", mDispatchId);
            extraDatas.put("json", new Gson().toJson(mNotesList));
            mHomeActivity.mintlogEventExtraData("Notes Comments List", extraDatas);

            if (!isCircumstanceCheck) {
                String time = getArguments().getString(NccConstants.CURRENT_TIME, "");
                List<ExtenuationCircumstancesItem> ExtenuationCircumstancesItem = jobDetail.getExtenuationCircumstances();
                if (ExtenuationCircumstancesItem != null) {
                    for (ExtenuationCircumstancesItem mExtenuationCircumstancesItem : ExtenuationCircumstancesItem) {
                        Comment comment1 = new Comment();
                        comment1.setUserName(getString(R.string.note_text_agero));
                        comment1.setCommentText(mExtenuationCircumstancesItem.getExtenuationDetailText());
                        comment1.setServerTimeUtc(mDispatchCreatedDate);
                        this.mExtenuationCircumstance.add(comment1);
                    }
                    // mCommentListAll.addAll(mExtenuationCircumstance);
                }
            }

            if (mExtenuationCircumstance != null) {
                HashMap<String, String> extraDatas1 = new HashMap<>();
                extraDatas1.put("dispatch_id", mDispatchId);
                extraDatas1.put("json", new Gson().toJson(mExtenuationCircumstance));
                mHomeActivity.mintlogEventExtraData("Notes Extenuation Circumstances List", extraDatas1);
                mCommentListAll.addAll(mExtenuationCircumstance);
            }
            sortList(mNotesList);
            mCommentListAll.addAll(mNotesList);

        } catch (Exception e) {
            mHomeActivity.mintLogException(e);
        }
        if (mCommentListAll.size() == 0) {
            mTextNotesDesc.setVisibility(View.VISIBLE
            );
        } else {
            mTextNotesDesc.setVisibility(View.GONE);
        }

//        sortList();
        mNotesListAdapter =
                new NotesListAdapter(getActivity(), mCommentListAll,mTimezone);
        mRecyclerNotes.setAdapter(mNotesListAdapter);
    }

    private void sortList(ArrayList commentsList) {
        Collections.sort(commentsList, (Comment c1, Comment c2) -> {
            return c1.getServerTimeUtc().compareTo(c2.getServerTimeUtc());
        });

    }

    public NotesFragment() {
        // Intentionally empty
    }

    public static NotesFragment newInstance(boolean isItFromHistory, boolean isEditable, boolean isItFromExtenCircumstances, String mDispatchId, String mDispatchCreatedDate, String jobString) {
        NotesFragment fragment = new NotesFragment();
        Bundle args = new Bundle();
        args.putBoolean(NccConstants.IS_IT_FROM_HISTORY, isItFromHistory);
        args.putBoolean(NccConstants.IS_IT_FROM_EXTENUATION_CIRCUMSTANCES, isItFromExtenCircumstances);
        args.putString(NccConstants.DISPATCH_REQUEST_NUMBER, mDispatchId);
        args.putString(NccConstants.DISPATCH_CREATED_DATE, mDispatchCreatedDate);
        args.putBoolean(NccConstants.IS_EDITABLE, isEditable);
        args.putString(NccConstants.JOB_JSON, jobString);

        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View superView = super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup fragment_content = superView.findViewById(R.id.fragment_content);

        view = inflater.inflate(R.layout.fragment_notes, fragment_content, true);
        unbinder = ButterKnife.bind(this, view);
        mHomeActivity = (HomeActivity) getActivity();
        mHomeActivity.hideBottomBar();
        mHomeActivity.showToolbar(getString(R.string.title_notes));
        mRecyclerNotes.setLayoutManager(new LinearLayoutManager(getActivity()));
        mNotesList = new ArrayList<>();
        mUserError = new UserError();
//        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
//        linearLayoutManager.setStackFromEnd(false);
//        mRecyclerNotes.setLayoutManager(linearLayoutManager);

        if(NccConstants.IS_CHAT_ENABLED) {
            mView.setVisibility(View.GONE);
            mEditNotes.setVisibility(View.GONE);
            mTextAdd.setVisibility(View.GONE);
        } else {
            mView.setVisibility(View.VISIBLE);
            mEditNotes.setVisibility(View.VISIBLE);
            mTextAdd.setVisibility(View.VISIBLE);
        }
        mTextAdd.setEnabled(false);
        if (getArguments() != null) {
            mDispatchId = getArguments().getString(NccConstants.DISPATCH_REQUEST_NUMBER, "");
            mDispatchCreatedDate = getArguments().getString(NccConstants.DISPATCH_CREATED_DATE, "");
            isEditable = getArguments().getBoolean(NccConstants.IS_EDITABLE);
            String jobJson = getArguments().getString(NccConstants.JOB_JSON);
            mCurrentJob = new Gson().fromJson(jobJson, JobDetail.class);
            FirebaseDatabase database = FirebaseDatabase.getInstance();
            if (!getArguments().getBoolean(NccConstants.IS_IT_FROM_HISTORY)) {

                myRef = database.getReference("ActiveJobs/" + mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, "") + "/" + mDispatchId);
                //myRef.keepSynced(true);
                getNotesFromFirebase();
            } else {
                //myRef = database.getReference("InActiveJobs/" + mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, "") + "/" + mDispatchId);
                loadNotesList(mCurrentJob);
               // myRef.keepSynced(true);
//                mEditNotes.setVisibility(View.GONE);
//                mTextAdd.setVisibility(View.GONE);
//                mView.setVisibility(View.GONE);
            }

          /*  if (!isEditable) {
                mEditNotes.setVisibility(View.GONE);
                mTextAdd.setVisibility(View.GONE);
                mView.setVisibility(View.GONE);
            } else {
                mEditNotes.setVisibility(View.VISIBLE);
                mTextAdd.setVisibility(View.VISIBLE);
                mView.setVisibility(View.VISIBLE);
            }*/

            mEditNotes.addTextChangedListener(textwatcher);
            mTextAdd.setBackgroundResource(R.drawable.border_corner_history);
            GradientDrawable drawableNotes = (GradientDrawable) mTextAdd.getBackground();
            drawableNotes.setColor(getResources().getColor(R.color.createacc_button_disable_color));

        }

        return superView;
    }

    private void getNotesFromFirebase() {
        if (Utils.isNetworkAvailable()) {
            showProgress();
            TokenManager.getInstance().validateToken(mHomeActivity, new TokenManager.TokenReponseListener() {
                @Override
                public void onRefreshSuccess() {
                    if (isAdded()) {
                        myRef.addValueEventListener(valueEventListener);
                    }
                }

                @Override
                public void onRefreshFailure() {
                    hideProgress();
                    if (isAdded()) {
                        mHomeActivity.tokenRefreshFailed();
                    }
                }
            });
        } else {
            hideProgress();
            mUserError.title = ""; mUserError.message = getString(R.string.network_error_message);
            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
        }

    }

    private void sendNotesToServer(ArrayList<Comment> mNotesList) {
        if (Utils.isNetworkAvailable()) {
            showProgress();
            TokenManager.getInstance().validateToken(mHomeActivity, new TokenManager.TokenReponseListener() {
                @Override
                public void onRefreshSuccess() {
                    if (isAdded()) {

                        HashMap<String, String> extraDatas = new HashMap<>();
                        extraDatas.put("dispatch_id", mDispatchId);
                        extraDatas.put("notes", new Gson().toJson(mNotesList));
                        mHomeActivity.mintlogEventExtraData("Notes Sending List", extraDatas);

                        RxFirebaseDatabase.setValue(myRef.child("comments"), mNotesList).subscribe(() -> {
                            hideProgress();
                        }, throwable -> {
                            hideProgress();
                            if (isAdded()) {
                                mHomeActivity.mintlogEvent("Notes Send Comment Firebase Database Error - " + throwable.getLocalizedMessage());
                            }
                        });

                    }
                }

                @Override
                public void onRefreshFailure() {
                    hideProgress();
                    if (isAdded()) {
                        mHomeActivity.tokenRefreshFailed();
                    }
                }
            });
        } else {
            hideProgress();
            mUserError.title = ""; mUserError.message = getString(R.string.network_error_message);
            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
        }
    }


    TextWatcher textwatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            GradientDrawable drawableNotes = (GradientDrawable) mTextAdd.getBackground();
            if (!mEditNotes.getText().toString().trim().isEmpty()) {
                mTextAdd.setEnabled(true);
                drawableNotes.setColor(getResources().getColor(R.color.createacc_enable_button_color));
                mTextAdd.setTextColor(getResources().getColor(R.color.ncc_white));
            } else {
                mTextAdd.setEnabled(false);
                drawableNotes.setColor(getResources().getColor(R.color.createacc_button_disable_color));
                mTextAdd.setTextColor(getResources().getColor(R.color.createacc_text_button_disbale_color));

            }
        }
    };

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        try {
            if (unbinder != null) {
                unbinder.unbind();
            }
            if (myRef != null) {
                myRef.removeEventListener(valueEventListener);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        try {
            if (myRef != null) {
                myRef.removeEventListener(valueEventListener);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            if (myRef != null) {
                myRef.removeEventListener(valueEventListener);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.text_add)
    public void onViewClicked() {
        isCircumstanceCheck = true;
        notes = new Comment("", TokenManager.getInstance().getDeviceId(), getString(R.string.app_name), mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""), mPrefs.getString(NccConstants.SIGNIN_USER_NAME, ""), mEditNotes.getText().toString().trim(), "DLC", DateTimeUtils.getUtcTimeFormat(), "FIREBASE", NccConstants.SUB_SOURCE);
        mNotesList.add(notes);
        mEditNotes.setText("");
        sendNotesToServer(mNotesList);
        UiUtils.hideKeyboard(view);
    }


}
