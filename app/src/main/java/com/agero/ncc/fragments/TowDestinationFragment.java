package com.agero.ncc.fragments;


import android.content.DialogInterface;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.agero.ncc.BuildConfig;
import com.agero.ncc.R;
import com.agero.ncc.activities.BaseActivity;
import com.agero.ncc.activities.HomeActivity;
import com.agero.ncc.adapter.TowDestinationAdapter;
import com.agero.ncc.app.NCCApplication;
import com.agero.ncc.model.AdditionalParameters;
import com.agero.ncc.model.DisablementLocation;
import com.agero.ncc.model.JobDetail;
import com.agero.ncc.model.TermsAndConditionsModel;
import com.agero.ncc.model.TowDestinationEntity;
import com.agero.ncc.model.TowDestinationsModel;
import com.agero.ncc.model.TowDestinationsRequest;
import com.agero.ncc.model.TowLocation;
import com.agero.ncc.model.TowLocationUpdateModel;
import com.agero.ncc.model.Vehicle;
import com.agero.ncc.retrofit.NccApi;
import com.agero.ncc.utils.DateTimeUtils;
import com.agero.ncc.utils.NccConstants;
import com.agero.ncc.utils.TokenManager;
import com.agero.ncc.utils.UserError;
import com.agero.ncc.utils.Utils;
import com.agero.ncc.views.RecyclerItemClickListener;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.agero.ncc.firebasefunctions.FirebaseFunctions;
import com.agero.ncc.firebasefunctions.FirebaseFunctionsException;
import com.agero.ncc.firebasefunctions.HttpsCallableResult;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import durdinapps.rxfirebase2.RxFirebaseDatabase;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class TowDestinationFragment extends BaseFragment implements HomeActivity.ToolbarSaveListener {

    HomeActivity mHomeActivity;
    @BindView(R.id.text_select_tow_destination)
    TextView mTextSelectTowDestination;
    @BindView(R.id.recycler_tow_destination)
    RecyclerView mRecyclerTowDestination;
    @BindView(R.id.text_custom_location)
    TextView mTextCustomLocation;
    @BindView(R.id.image_enter_location)
    ImageView mImageEnterLocation;
    @BindView(R.id.constraint_enter_location)
    ConstraintLayout mConstraintEnterLocation;
    private TowDestinationAdapter towDestinationAdapter;
    @Inject
    NccApi nccApi;
    private UserError mUserError;
    private long oldRetryTime = 0;
    private String mUsername, mDispatchNumber;
    private DatabaseReference mJobReference;
    private JobDetail mCurrentJob;

    private static final String HEADER_API_KEY = "PxjP1h4RRcKC2G5nS19ZkEeWwlSBsXLG";
    private static final String HEADER_CONTENT_TYPE = "application/json";
    private static final String HEADER_AGERO_PROMOCODE = "FOR";

    public static TowDestinationFragment newInstance(String dispatchNumber, String userName) {
        TowDestinationFragment fragment = new TowDestinationFragment();
        Bundle bundle = new Bundle();
        bundle.putString(NccConstants.DISPATCHER_NUMBER, dispatchNumber);
        bundle.putString(NccConstants.USERNAME, userName);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View superView = super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup fragment_content = superView.findViewById(R.id.fragment_content);
        View view = inflater.inflate(R.layout.fragment_tow_destination, fragment_content, true);
        ButterKnife.bind(this, view);
        mHomeActivity = (HomeActivity) getActivity();
        mHomeActivity.showToolbar(getResources().getString(R.string.title_edit_tow));
        mHomeActivity.saveDisable();
        mHomeActivity.setOnToolbarSaveListener(this);
        NCCApplication.getContext().getComponent().inject(this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setAutoMeasureEnabled(true);
        mRecyclerTowDestination.setLayoutManager(linearLayoutManager);
        mUserError = new UserError();
        mUsername = getArguments().getString(NccConstants.USERNAME);
        mDispatchNumber = getArguments().getString(NccConstants.DISPATCHER_NUMBER);

        getJobDetails();

        return superView;
    }

    private void getJobDetails() {
        if (Utils.isNetworkAvailable()) {
            showProgress();
            TokenManager.getInstance().validateToken(mHomeActivity, new TokenManager.TokenReponseListener() {
                @Override
                public void onRefreshSuccess() {
                    if (isAdded()) {
                        hideProgress();
                        FirebaseDatabase database = FirebaseDatabase.getInstance();
                        mJobReference = database.getReference("ActiveJobs/"
                                + mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, "") + "/" + mDispatchNumber);
                        //mJobReference.keepSynced(true);
                        RxFirebaseDatabase.observeSingleValueEvent(mJobReference, JobDetail.class).subscribe(taskSnapshot -> {
                            if (isAdded()) {
                                if (taskSnapshot != null) {

                                    HashMap<String, String> extraDatas = new HashMap<>();
                                    extraDatas.put("json", new Gson().toJson(taskSnapshot));
                                    mHomeActivity.mintlogEventExtraData("Tow Destinations Job Details", extraDatas);

                                    mCurrentJob = taskSnapshot;
                                    getTowDestionations();

                                }

                            }

                        }, throwable -> {
                            if (isAdded()) {
                                mHomeActivity.mintlogEvent("Tow Destinations Job Details Firebase Database Error - " + throwable.getLocalizedMessage());
                                hideProgress();
                                if (oldRetryTime == 0 || (System.currentTimeMillis() - oldRetryTime) > NccConstants.RETRY_LIMIT_MILLI_SEC) {
                                    getJobDetails();
                                    oldRetryTime = System.currentTimeMillis();
                                }
                            }
                        });

                    }
                }


                @Override
                public void onRefreshFailure() {
                    if (isAdded()) {
                        hideProgress();
                        mHomeActivity.tokenRefreshFailed();
                    }
                }
            });
        } else {
            mUserError.title = ""; mUserError.message = getString(R.string.network_error_message);
            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
        }

    }

    private void getTowDestionations() {
        if (Utils.isNetworkAvailable()) {
            showProgress();
            TowDestinationsRequest towDestinationsRequest = new TowDestinationsRequest();

            Vehicle vehicle = mCurrentJob.getVehicle();
            AdditionalParameters additionalParameters = new AdditionalParameters("FOR", "true", "LIW", "false", "true", "00115");

            towDestinationsRequest.setVehicleModel(vehicle.getModel());
            towDestinationsRequest.setVehicleMake(vehicle.getMake());
            towDestinationsRequest.setVehicleYear("" + vehicle.getYear());
            towDestinationsRequest.setCallType(4);
            towDestinationsRequest.setAdditionalParameters(additionalParameters);
            towDestinationsRequest.setDisablementReason(mCurrentJob.getServiceTypeCode());
            towDestinationsRequest.setLatitude("" + mCurrentJob.getDisablementLocation().getLatitude());
            towDestinationsRequest.setLongitude("" + mCurrentJob.getDisablementLocation().getLongitude());
            towDestinationsRequest.setPostalCode(mCurrentJob.getDisablementLocation().getPostalCode());
            towDestinationsRequest.setState(mCurrentJob.getDisablementLocation().getState());

            nccApi.getTowDestinations(BuildConfig.TOW_DESTIONATIONS_URL, HEADER_API_KEY, HEADER_CONTENT_TYPE, HEADER_AGERO_PROMOCODE, towDestinationsRequest).enqueue(new Callback<TowDestinationsModel>() {
                @Override
                public void onResponse(Call<TowDestinationsModel> call, Response<TowDestinationsModel> response) {
                    if (isAdded()) {
                        hideProgress();
                        if (response != null && response.code() == 200) {
                            hideProgress();
                            TowDestinationsModel towDestinationsModel = response.body();

                            HashMap<String, String> extraDatas = new HashMap<>();
                            extraDatas.put("json", new Gson().toJson(towDestinationsModel));
                            mHomeActivity.mintlogEventExtraData("Tow Destinations List", extraDatas);

                            if (towDestinationsModel != null && towDestinationsModel.getTowDestinations() != null && towDestinationsModel.getTowDestinations().size() > 0) {
                                towDestinationAdapter = new TowDestinationAdapter(mHomeActivity, towDestinationsModel.getTowDestinations());
                                mRecyclerTowDestination.setAdapter(towDestinationAdapter);
                            } else {
                                mHomeActivity.push(EditTowDisablementFragment.newInstance(false, mDispatchNumber, mUsername), getString(R.string.title_edit_tow));
                            }


                        }
                    }
                }

                @Override
                public void onFailure(Call<TowDestinationsModel> call, Throwable t) {
                    hideProgress();
                    Timber.d("Tow Destinations List Api Error - " + t.getLocalizedMessage());
                    mHomeActivity.mintlogEvent("Tow Destinations List Api Error - " + t.getLocalizedMessage());
                }
            });
        } else {
            mUserError.title = ""; mUserError.message = getString(R.string.network_error_message);
            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
        }
    }


    @Override
    public void onSave() {
        towLocationAlertDialog(towDestinationAdapter.getSelectedItem());
    }

    private void saveLocation(TowDestinationEntity selectedItem) {
        if (Utils.isNetworkAvailable()) {
            showProgress();
            TokenManager.getInstance().validateToken(((BaseActivity) getActivity()), new TokenManager.TokenReponseListener() {
                @Override
                public void onRefreshSuccess() {
                    if (isAdded()) {
                        TowLocation newTowLocation = mCurrentJob.getTowLocation();

                        newTowLocation.setAddressLine1(selectedItem.getAddress());
                        newTowLocation.setAddressLine2("");
                        newTowLocation.setCity(selectedItem.getCity());
                        newTowLocation.setState(selectedItem.getState());
                        newTowLocation.setPostalCode(selectedItem.getPostalCode());

                        newTowLocation.setLatitude(selectedItem.getLatitude());
                        newTowLocation.setLongitude(selectedItem.getLongitude());
                        newTowLocation.setUserId(mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""));
                        newTowLocation.setUserName(mPrefs.getString(NccConstants.SIGNIN_USER_NAME, ""));
                        newTowLocation.setStatusTime(DateTimeUtils.getUtcTimeFormat());
                        newTowLocation.setServerTimeUtc(DateTimeUtils.getUtcTimeFormat());
                        newTowLocation.setSource("FIREBASE");
                        newTowLocation.setSubSource(NccConstants.SUB_SOURCE);
                        newTowLocation.setDeviceId(Settings.Secure.getString(NCCApplication.getContext().getContentResolver(), Settings.Secure.ANDROID_ID));

                        HashMap<String, String> extraDatas = new HashMap<>();
                        extraDatas.put("tow_location", new Gson().toJson(newTowLocation));
                        extraDatas.put("dispatch_id", mDispatchNumber);
                        ((HomeActivity) getActivity()).mintlogEventExtraData("Update Tow Address", extraDatas);

                        updateTowLocationAPI(newTowLocation);
                    }
                }

                @Override
                public void onRefreshFailure() {
                    hideProgress();
                    if(isAdded()) {
                    mHomeActivity.tokenRefreshFailed();
                    }
                }
            });

        } else {
            mUserError.title = ""; mUserError.message = getString(R.string.network_error_message);
            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
        }


    }

    private void updateTowLocationAPI(TowLocation newTowLocation) {
        TowLocationUpdateModel disablementLocationUpdateModel = new TowLocationUpdateModel(
                newTowLocation,
                mPrefs.getString(NccConstants.APIGEE_TOKEN, ""),
                DateTimeUtils.getUtcTimeFormat(),
                NccConstants.SUB_SOURCE,
                mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, ""),
                mPrefs.getString(NccConstants.SIGNIN_USER_ID, "")
        );
        FirebaseFunctions.getInstance()
                .getHttpsCallable(NccConstants.API_JOB_TL_UPDATE)
                .call(new Gson().toJson(disablementLocationUpdateModel))
                .addOnFailureListener(new OnFailureListener() {

                    @Override
                    public void onFailure(@NonNull Exception e) {
                        hideProgress();
                        Timber.d("failed");
//                                    e.printStackTrace();
                        UserError mUserError = new UserError();
                        if (e instanceof FirebaseFunctionsException) {
                            FirebaseFunctionsException ffe = (FirebaseFunctionsException) e;
                            FirebaseFunctionsException.Code code = ffe.getCode();
                            Object details = ffe.getDetails();
//                                        Toast.makeText(((BaseActivity) getActivity()), ffe.getMessage(), Toast.LENGTH_LONG).show();
                            ((HomeActivity) getActivity()).mintlogEvent("Disablement Location Update: " + " Firebase Functions Error - Error Message -" + ffe.getMessage());
                                mUserError.message = ffe.getMessage();
                                try {
                                    JSONObject errorObj = new JSONObject(ffe.getMessage());
                                    mUserError.title = errorObj.optString("title");
                                    mUserError.message = errorObj.optString("body");
                                    showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);

                                } catch (Exception parseException) {
                                    parseException.printStackTrace();
                                    showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
                                }
                                showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);

                        } else if (e.getMessage() != null) {
                            if(getString(R.string.firebase_logout_error_msg).equalsIgnoreCase(e.getMessage())){
                                ((HomeActivity) getActivity()).tokenRefreshFailed();
                            }else {
                                mUserError.message = e.getMessage();
                                showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
                                ((HomeActivity) getActivity()).mintlogEvent("Disablement Location Update: " + " Firebase Functions Error - Error Message -" + e.getMessage());
                            }
                        } else {
                            ((HomeActivity) getActivity()).mintlogEvent("Disablement Location Update Firebase Functions Error - Error Message -  failed");
                            mUserError.message = "failed";
                            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
                        }
                    }
                })
                .addOnSuccessListener(new OnSuccessListener<HttpsCallableResult>() {
                    @Override
                    public void onSuccess(HttpsCallableResult httpsCallableResult) {
                        hideProgress();
                        mHomeActivity.popBackStackImmediate();
                    }
                });
    }

    private void towLocationAlertDialog(TowDestinationEntity selectedItem) {

        String originalAddress = mCurrentJob.getTowLocation().getAddressLine1() + ", " + mCurrentJob.getTowLocation().getCity() + ", " + mCurrentJob.getTowLocation().getPostalCode();

        String changedAddress = selectedItem.getAddress() + ", "+selectedItem.getCity()+", "+ selectedItem.getPostalCode();

        Date date = new Date();
        SimpleDateFormat simpleFormater = new SimpleDateFormat("E, MMM d, hh:mm a", Locale.US);

        AlertDialogFragment alert = AlertDialogFragment.newDialog(Html.fromHtml(getString(R.string.lable_tow_destination_changed)), Html.fromHtml("<font color='#999999'>" + "Address: " + changedAddress + ". Requested by " + mUsername + " " + getString(R.string.dot) + " " + simpleFormater.format(date) + "<br><br>" + "Original Address: " + originalAddress + "</font>")
                , "", Html.fromHtml("<b>" + getString(R.string.ok) + "</b>"));

        alert.setListener(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                saveLocation(selectedItem);

                dialogInterface.dismiss();
            }
        });
        alert.show(getFragmentManager().beginTransaction(), EditTowDisablementFragment.class.getClass().getCanonicalName());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @OnClick({R.id.constraint_enter_location})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.constraint_enter_location:
                if(isAdded()){
                    mHomeActivity.push(EditTowDisablementFragment.newInstance(false, mDispatchNumber, mUsername), getString(R.string.title_edit_tow));
                }
                break;
        }
    }
}
