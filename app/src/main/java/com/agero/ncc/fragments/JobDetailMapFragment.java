package com.agero.ncc.fragments;

import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.OnClick;
import com.agero.ncc.R;
import com.agero.ncc.activities.BaseActivity;
import com.agero.ncc.activities.HomeActivity;
import com.agero.ncc.app.NCCApplication;
import com.agero.ncc.model.Facility;
import com.agero.ncc.model.JobDetail;
import com.agero.ncc.utils.DownloadDirections;
import com.agero.ncc.utils.NccConstants;
import com.agero.ncc.utils.UserError;
import com.agero.ncc.utils.Utils;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Dash;
import com.google.android.gms.maps.model.Gap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PatternItem;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.Arrays;

import java.util.List;

import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

import static com.agero.ncc.utils.NccConstants.JOB_DISPATCH_SOURCE_ASM_VRM;


/**
 * Job details with map showing direction to disablement from towTo location.
 */
public class JobDetailMapFragment extends BaseFragment implements OnMapReadyCallback {
    protected HomeActivity mHomeActivity;
    protected GoogleMap mMap;
    protected PolylineOptions mPolyLineOptions, mFacilityPolyLineOptions;
    protected Marker mCurrentLocationMarker;
    private static final PatternItem DASH = new Dash(20);
    private static final PatternItem GAP = new Gap(10);
    private static final List<PatternItem> PATTERN_POLYLINE_DOTTED = Arrays.asList(GAP, DASH);

    @BindView(R.id.text_disablement_address)
    TextView mTextDisablementAddress;
    @BindView(R.id.text_disablement_status)
    TextView mTextDisablementStatus;
    @BindView(R.id.text_disablement_distance)
    TextView mTextDisablementDistance;
    @BindView(R.id.text_vertical_line_disablement)
    View mDisablementSeparatorLine;
    @BindView(R.id.text_tow_to_address)
    TextView mTextTowAddress;
    @BindView(R.id.text_tow_destination_distance)
    TextView mTextTowDistance;
    @BindView(R.id.constraint_tow_to)
    ConstraintLayout mTowLayout;
    @BindView(R.id.view_border_tow)
    View mTowSeparatorLine;
    @BindView(R.id.card_joblist)
    CardView mRouteDetailsCardView;
    @BindView(R.id.image_tow_direction)
    ImageView mImageTowDirection;
    protected static JobDetail mCurrentJob;

    private CompositeDisposable disposables = new CompositeDisposable();
//    @BindView(R.id.text_get_directions)
//    TextView mTextGetDirections;

    private boolean mIsTowJob;
    private LatLng mTowLatLng, mDisablementLatLng, mCurrentLocationLatLng;

    public JobDetailMapFragment() {
        // Required empty public constructor
    }

    public static JobDetailMapFragment newInstance(JobDetail job, Facility facility,
            PolylineOptions polylineOptions, PolylineOptions facilityPolylineOptions) {
        JobDetailMapFragment fragment = new JobDetailMapFragment();
        Bundle bundle = new Bundle();
        mCurrentJob = job;
        if(job != null && job.getDisablementLocation() != null)
        {
            bundle.putDouble("disablementLat",
                    job.getDisablementLocation().getLatitude() == null ? 0.0 : job.getDisablementLocation().getLatitude());
            bundle.putDouble("disablementLng",
                    job.getDisablementLocation().getLongitude() == null ? 0.0 : job.getDisablementLocation().getLongitude());
        }else{
            bundle.putDouble("disablementLat", 0.0);
            bundle.putDouble("disablementLng", 0.0);
        }

        if(job != null && job.getTowLocation() != null) {

            bundle.putDouble("towLat",
                    job.getTowLocation().getLatitude() == null ? 0.0 : job.getTowLocation().getLatitude());
            bundle.putDouble("towLng",
                    job.getTowLocation().getLongitude() == null ? 0.0 : job.getTowLocation().getLongitude());
        }else{
            bundle.putDouble("towLat", 0.0);
            bundle.putDouble("towLng", 0.0);
        }

        if(facility != null && facility.getAddressList() != null && facility.getAddressList().size() > 0 && facility.getAddressList().get(0).getLatitude() != null && facility.getAddressList().get(0).getLongitude() != null) {
            bundle.putDouble("facilityLat",
                    facility.getAddressList().get(0).getLatitude());
            bundle.putDouble("facilityLng",
                    facility.getAddressList().get(0).getLongitude());
        }else{
            bundle.putDouble("facilityLat", 0.0);
            bundle.putDouble("facilityLng", 0.0);
        }
        bundle.putParcelable("PolyLines", polylineOptions);
        bundle.putParcelable("facilityPolyLines", facilityPolylineOptions);

        if (job.getServiceTypeCode() != null  && job.getTowLocation() != null && Utils.isTow(job.getServiceTypeCode())) {

            StringBuilder towAddress = new StringBuilder(job.getTowLocation().getAddressLine1());
            towAddress.append(", ");
            towAddress.append(job.getTowLocation().getCity());
            towAddress.append(", ");
            towAddress.append(job.getTowLocation().getState());

            bundle.putString("towAddress", towAddress.toString());
            bundle.putString("towDistance", ""+job.getMilesToTowLoc());
        }

        StringBuilder disablementAddress =
                new StringBuilder(job.getDisablementLocation().getAddressLine1());
        disablementAddress.append(", ");
        disablementAddress.append(job.getDisablementLocation().getCity());
        disablementAddress.append(", ");
        disablementAddress.append(job.getDisablementLocation().getState());

        bundle.putString("disablementAddress", disablementAddress.toString());
        bundle.putString("disablementStatus", "En-Route");
        bundle.putString("disablementDistance", ""+job.getMilesToDisablementLoc());
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {

        View superView = super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup fragment_content = superView.findViewById(R.id.fragment_content);

        View view = inflater.inflate(R.layout.fragment_job_detail_map, fragment_content, true);
        ButterKnife.bind(this, view);

        mPolyLineOptions = getArguments().getParcelable("PolyLines");
        mFacilityPolyLineOptions = getArguments().getParcelable("facilityPolyLines");
        mHomeActivity = (HomeActivity) getActivity();
        mHomeActivity.hideBottomBar();
        mHomeActivity.showToolbar(getString(R.string.title_job_detail_map));
        mIsTowJob = false;
//        if (mHomeActivity.isLoggedInDriver()) {
//            mRouteDetailsCardView.setVisibility(View.VISIBLE);
//        } else {
            mTextDisablementAddress.setText(Utils.toCamelCase(getArguments().getString("disablementAddress")));
            mTextDisablementStatus.setText(getArguments().getString("disablementStatus"));

            mTextDisablementDistance.setText(
                    new StringBuilder(" ").append(getArguments().getString("disablementDistance"))
                            .append(" mi"));

            if (getArguments().containsKey("towAddress")) {
                mIsTowJob = true;
                mTextTowAddress.setText(Utils.toCamelCase(getArguments().getString("towAddress")));
                mTextTowDistance.setText(
                        new StringBuilder("Towed ").append(getArguments().getString("towDistance"))
                                .append(" mi"));
            } else {
                mTowLayout.setVisibility(View.GONE);
                mTowSeparatorLine.setVisibility(View.GONE);
                mDisablementSeparatorLine.setVisibility(View.GONE);
                mImageTowDirection.setVisibility(View.GONE);
            }
//        }

        loadMap();
        return superView;
    }

    protected void loadMap() {
        SupportMapFragment supportMapFragment = new SupportMapFragment();
        getChildFragmentManager().beginTransaction()
                .add(R.id.frame_map_container, supportMapFragment)
                .commit();
        supportMapFragment.getMapAsync(this);
    }

    @Override public void onMapReady(GoogleMap googleMap) {
        if (isAdded()) {
            mMap = googleMap;
            LatLngBounds.Builder latLngBuilder = new LatLngBounds.Builder();
            mDisablementLatLng = new LatLng(getArguments().getDouble("disablementLat"),
                    getArguments().getDouble("disablementLng"));

            MarkerOptions disablementMarkerOptions = new MarkerOptions();
            disablementMarkerOptions.position(mDisablementLatLng);
            if (!TextUtils.isEmpty(mCurrentJob.getDispatchSource()) && JOB_DISPATCH_SOURCE_ASM_VRM.equalsIgnoreCase(mCurrentJob.getDispatchSource()) && (mCurrentJob.getServiceTypeCode().toUpperCase().startsWith(NccConstants.JOB_SERVICE_TYPE_ASM))) {
                disablementMarkerOptions.icon(
                        BitmapDescriptorFactory.fromResource(R.mipmap.ico_asm_towto));
            } else {
                disablementMarkerOptions.icon(
                        BitmapDescriptorFactory.fromResource(R.mipmap.ic_map_disablement));
            }

            mMap.addMarker(disablementMarkerOptions);

            latLngBuilder.include(mDisablementLatLng);

            if (getArguments().getDouble("towLat") != 0.0) {
                mTowLatLng = new LatLng(getArguments().getDouble("towLat"),
                        getArguments().getDouble("towLng"));

                MarkerOptions towToMarkerOptions = new MarkerOptions();
                towToMarkerOptions.position(mTowLatLng);
                towToMarkerOptions.icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_map_towto));
                mMap.addMarker(towToMarkerOptions);


                latLngBuilder.include(mTowLatLng);


                if (mPolyLineOptions != null) {
                    mMap.addPolyline(mPolyLineOptions);
                } else {

                    mPolyLineOptions = new PolylineOptions();
                    DownloadDirections downloadDirectionsTask =
                            new DownloadDirections(
                                    new DownloadDirections.DirectionDownloadedListener() {
                                        @Override
                                        public void onSuccess(PolylineOptions polylineOptions) {
                                            if (isAdded()) {
                                                mPolyLineOptions = polylineOptions;
                                                mMap.addPolyline(mPolyLineOptions);
                                            }
                                        }

                                        @Override
                                        public void onFailure() {
                                            if (isAdded()) {
                                                mHomeActivity.showError(
                                                        BaseActivity.Companion.getERROR_SHOW_TYPE_TOAST(),
                                                        new UserError("", "",
                                                                getString(R.string.map_error_directions)));
                                            }
                                        }
                                    });
                    Disposable dirDisposables = downloadDirectionsTask.execute(Utils.getDirectionsUrl(mDisablementLatLng, mTowLatLng));
                    disposables.add(dirDisposables);
                }
            }

            if (getArguments() != null && getArguments().getDouble("facilityLat") != 0.0) {
                LatLng facilityLatLng = new LatLng(getArguments().getDouble("facilityLat"),
                        getArguments().getDouble("facilityLng"));

                MarkerOptions towToMarkerOptions = new MarkerOptions();
                towToMarkerOptions.position(facilityLatLng);
                towToMarkerOptions.icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_map_facility));
                mMap.addMarker(towToMarkerOptions);

                latLngBuilder.include(facilityLatLng);

//            CameraPosition camPos =
//                    new CameraPosition.Builder().target(latLngBounds.getCenter()).zoom(9).build();
//            CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(camPos);
//            mMap.moveCamera(cameraUpdate);

                if (mFacilityPolyLineOptions != null) {
                    if (isAdded()) {
                        Polyline polyline = mMap.addPolyline(mFacilityPolyLineOptions);
                        if ((mFacilityPolyLineOptions.getPattern() == null)
                                || (!mFacilityPolyLineOptions.getPattern().contains(DASH))) {
                            polyline.setPattern(PATTERN_POLYLINE_DOTTED);
                            polyline.setColor(getResources().getColor(R.color.ncc_gray_666));
                        }
                    }
                } else {

                    mFacilityPolyLineOptions = new PolylineOptions();
                    DownloadDirections downloadDirectionsTask =
                            new DownloadDirections(
                                    new DownloadDirections.DirectionDownloadedListener() {
                                        @Override
                                        public void onSuccess(PolylineOptions polylineOptions) {
                                            if (isAdded()) {
                                                mFacilityPolyLineOptions = polylineOptions;
                                                Polyline polyline =
                                                        mMap.addPolyline(mFacilityPolyLineOptions);
                                                if ((mFacilityPolyLineOptions.getPattern() == null)
                                                        || (!mFacilityPolyLineOptions.getPattern()
                                                        .contains(DASH))) {
                                                    polyline.setPattern(PATTERN_POLYLINE_DOTTED);
                                                    polyline.setColor(getResources().getColor(R.color.ncc_gray_666));
                                                }
                                            }
                                        }

                                        @Override
                                        public void onFailure() {
                                            if (isAdded()) {
                                                mHomeActivity.showError(
                                                        BaseActivity.Companion.getERROR_SHOW_TYPE_TOAST(),
                                                        new UserError("", "",
                                                                getString(R.string.map_error_directions)));
                                            }
                                        }
                                    });
                    Disposable directionDisposable = downloadDirectionsTask.execute(
                            Utils.getDirectionsUrl(facilityLatLng, mDisablementLatLng));
                    disposables.add(directionDisposable);

                }
            }

            Observable<Location> locationObservable = mHomeActivity.getLocationObservable();

            if (NCCApplication.mLocationData != null) {
                mCurrentLocationLatLng =
                        new LatLng(NCCApplication.mLocationData.getLatitude(), NCCApplication.mLocationData.getLongitude());
                MarkerOptions currentLocationMarkerOptions = new MarkerOptions();
                currentLocationMarkerOptions.position(mCurrentLocationLatLng);
                currentLocationMarkerOptions.icon(
                        BitmapDescriptorFactory.fromResource(R.mipmap.ic_pin_current_location));
                mMap.addMarker(currentLocationMarkerOptions);
            }

            locationObservable.subscribe(location -> {
                if (mMap != null) {
                    mCurrentLocationLatLng =
                            new LatLng(location.getLatitude(), location.getLongitude());
                    MarkerOptions currentLocationMarkerOptions = new MarkerOptions();
                    currentLocationMarkerOptions.position(mCurrentLocationLatLng);
                    currentLocationMarkerOptions.icon(
                            BitmapDescriptorFactory.fromResource(R.mipmap.ic_pin_current_location));
                    if (mCurrentLocationMarker != null) {
                        mCurrentLocationMarker.remove();
                    }
                    mCurrentLocationMarker = mMap.addMarker(currentLocationMarkerOptions);
                }
            });

            if (mCurrentLocationLatLng != null && mCurrentLocationLatLng.latitude != 0.0 && mCurrentLocationLatLng.longitude != 0.0) {
                latLngBuilder.include(mCurrentLocationLatLng);
            }
            LatLngBounds latLngBounds = latLngBuilder.build();
            float value = getResources().getDimension(R.dimen.job_details_map_height);
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(latLngBounds, (int) value / 3);

            mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                @Override
                public void onMapLoaded() {
                    /**set animated zoom camera into map*/
                    mMap.animateCamera(cu);
                }
            });
        }
    }

    @OnClick({ R.id.image_disablement_direction,R.id.image_tow_direction }) public void onClick(View view) {
        switch (view.getId()) {
            case R.id.image_disablement_direction:
                getDirection(false);
                break;
            case R.id.image_tow_direction:
                getDirection(true);
                break;
        }
    }

    public void getDirection(boolean mIsTowJob) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(
                "geo:<lat>,<long>?q=<" + (mIsTowJob ? mTowLatLng.latitude
                        : mDisablementLatLng.latitude) + ">,<" + (mIsTowJob ? mTowLatLng.longitude
                        : mDisablementLatLng.longitude) + ">"));
        startActivity(intent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(!disposables.isDisposed()){
            disposables.dispose();
        }

    }
}
