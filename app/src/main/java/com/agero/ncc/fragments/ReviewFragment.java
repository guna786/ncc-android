package com.agero.ncc.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.agero.ncc.R;
import com.agero.ncc.activities.BaseActivity;
import com.agero.ncc.activities.HomeActivity;
import com.agero.ncc.driver.fragments.ActiveJobsFragment;
import com.agero.ncc.model.JobDetail;
import com.agero.ncc.model.Vehicle;
import com.agero.ncc.utils.NccConstants;
import com.agero.ncc.utils.TokenManager;
import com.agero.ncc.utils.UserError;
import com.agero.ncc.utils.Utils;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class ReviewFragment extends BaseFragment {
    HomeActivity mHomeActivity;
    Unbinder unbinder;
    @BindView(R.id.text_job_id)
    TextView mTextJobId;
    @BindView(R.id.text_job_details)
    TextView mTextJobDetails;
    @BindView(R.id.text_edit_job_details)
    TextView mTextEditJobDetails;
    @BindView(R.id.text_job_type)
    TextView mTextJobType;
    @BindView(R.id.text_job_service)
    TextView mTextJobService;
    @BindView(R.id.text_disablement_address)
    TextView mTextDisablementAddress;
    @BindView(R.id.text_disablement_address_street)
    TextView mTextDisablementAddressStreet;
    @BindView(R.id.text_disablement_address_city_pincode)
    TextView mTextDisablementAddressCityPincode;
    @BindView(R.id.text_driver_vehicle)
    TextView mTextDriverVehicle;
    @BindView(R.id.text_passengers)
    TextView mTextPassengers;
    @BindView(R.id.text_coverage)
    TextView mTextCoverage;
    @BindView(R.id.text_urgency)
    TextView mTextUrgency;
    @BindView(R.id.text_tow_destination)
    TextView mTextTowDestination;
    @BindView(R.id.text_edit_tow_destination)
    TextView mTextEditTowDestination;
    @BindView(R.id.text_tow_destination_address)
    TextView mTextTowDestinationAddress;
    @BindView(R.id.text_tow_destination_address_street)
    TextView mTextTowDestinationAddressStreet;
    @BindView(R.id.text_tow_destination_address_city_pincode)
    TextView mTextTowDestinationAddressCityPincode;
    @BindView(R.id.text_night_drop_off)
    TextView mTextNightDropOff;
    @BindView(R.id.text_storage)
    TextView mTextStorage;
    @BindView(R.id.text_facility_hours)
    TextView mTextFacilityHours;
    @BindView(R.id.text_estimated_arrival_time)
    TextView mTextEstimatedArrivalTime;
    @BindView(R.id.text_vehicle_details)
    TextView mTextVehicleDetails;
    @BindView(R.id.text_edit_vehicle_details)
    TextView mTextEditVehicleDetails;
    @BindView(R.id.text_vehicle_details_name)
    TextView mTextVehicleDetailsName;
    @BindView(R.id.text_vin)
    TextView mTextVin;
    @BindView(R.id.text_type_class)
    TextView mTextTypeClass;
    @BindView(R.id.text_drivetrain)
    TextView mTextDrivetrain;
    @BindView(R.id.text_fuel_type)
    TextView mTextFuelType;
    @BindView(R.id.text_customer_details)
    TextView mTextCustomerDetails;
    @BindView(R.id.text_edit_customer_details)
    TextView mTextEditCustomerDetails;
    @BindView(R.id.text_cusomter_details_name)
    TextView mTextCusomterDetailsName;
    @BindView(R.id.text_cusomter_details_contact_number)
    TextView mTextCusomterDetailsContactNumber;
    @BindView(R.id.text_cusomter_details_payment)
    TextView mTextCusomterDetailsPayment;
    @BindView(R.id.text_comments)
    TextView mTextComments;
    @BindView(R.id.text_entered_comments)
    TextView mTextEnteredComments;
    @BindView(R.id.button_submit_job)
    Button mButtonSubmitJob;
    UserError mUserError;

    public ReviewFragment() {
        // Intentionally empty
    }

    public static ReviewFragment newInstance() {
        ReviewFragment fragment = new ReviewFragment();
        return fragment;

    }

    private DatabaseReference myRef;
    private ValueEventListener valueEventListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {

        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View superView = super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup fragment_content = superView.findViewById(R.id.fragment_content);

        View view = inflater.inflate(R.layout.fragment_review, fragment_content, true);
        unbinder = ButterKnife.bind(this, view);
        mHomeActivity = (HomeActivity) getActivity();
        mUserError = new UserError();
        mHomeActivity.hideBottomBar();
        mHomeActivity.showToolbar(getString(R.string.title_add_customer));
        addJobReview();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        myRef = database.getReference("ActiveJobs/" + mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, ""));
       // myRef.keepSynced(true);
        return superView;
    }

    @OnClick({R.id.button_submit_job, R.id.text_edit_job_details, R.id.text_edit_tow_destination, R.id.text_edit_vehicle_details, R.id.text_edit_customer_details})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.button_submit_job:

                submitJob();

                break;

            case R.id.text_edit_job_details:
                mHomeActivity.push(AddJobsDetailsFragment.newInstance(false), getString(R.string.title_add_job));
                break;

            case R.id.text_edit_tow_destination:
                mHomeActivity.push(AddTowDestinationFragment.newInstance(false), getString(R.string.title_add_tow));
                break;

            case R.id.text_edit_vehicle_details:
                mHomeActivity.push(AddVehicleDetailsFragment.newInstance(false), getString(R.string.title_add_vehicle));
                break;

            case R.id.text_edit_customer_details:
                mHomeActivity.push(AddCustomerDetailsFragment.newInstance(false), getString(R.string.title_add_customer));
                break;
        }
    }

    private void submitJob() {
        if (Utils.isNetworkAvailable()) {
            showProgress();
            TokenManager.getInstance().validateToken(mHomeActivity, new TokenManager.TokenReponseListener() {
                @Override
                public void onRefreshSuccess() {
                    if (isAdded()) {
                        myRef.child(AddJobsDetailsFragment.newJob.getDispatchId()).setValue(AddJobsDetailsFragment.newJob);
                        mHomeActivity.popAllBackstack();
                        mHomeActivity.push(ActiveJobsFragment.newInstance());
                    }
                }

                @Override
                public void onRefreshFailure() {
                    hideProgress();
                    if (isAdded()) {
                        mHomeActivity.tokenRefreshFailed();
                    }
                }
            });
        } else {
            mUserError.title = ""; mUserError.message = getString(R.string.network_error_message);
            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
        }

    }

    private void addJobReview() {
        JobDetail job = AddJobsDetailsFragment.newJob;

        //Job Id
        mTextJobId.setText("PO# " + job.getPoNumber());
        mTextJobType.setText(job.getServiceTypeCode());


        //Job Details
        mTextJobService.setText(job.getServiceTypeCode());
//        mTextJobType.setText(job.);

        //Disablement Address
        mTextDisablementAddressStreet.setText(job.getDisablementLocation().getAddressLine1());
        mTextDisablementAddressCityPincode.setText(job.getDisablementLocation().getCity() + ", " + job.getDisablementLocation().getState());

        mTextDriverVehicle.setText(getString(R.string.add_job_review_driver_vehicle) + " " + "Yes");

        mTextPassengers.setText(getString(R.string.add_job_review_passengers));
//        mTextCoverage.setText( getString(R.string.add_job_review_coverage) +" "+job.getService().getCoverage());
//        mTextUrgency.setText(getString(R.string.add_job_review_urgency)+" "+job.getService().getUrgency());


        //Tow Destination
        if (job.getServiceTypeCode().startsWith("Tow")) {
            mTextTowDestinationAddress.setText(job.getTowLocation().getLandmark().toString());
            mTextTowDestinationAddressStreet.setText(job.getTowLocation().getAddressLine1());
            mTextTowDestinationAddressCityPincode.setText(job.getTowLocation().getCity() + ", " + job.getTowLocation().getState());
            mTextNightDropOff.setText(getString(R.string.add_job_review_night_drop_off) + " " + job.getTowLocation().getNightDropOff());
            mTextStorage.setText(getString(R.string.add_job_review_storage));
            mTextFacilityHours.setText(getString(R.string.add_job_review_facility_hours));
            mTextEstimatedArrivalTime.setText(getString(R.string.add_job_review_estimated_arrival_time));
        } else {
            mTextTowDestination.setVisibility(View.GONE);
            mTextTowDestinationAddress.setVisibility(View.GONE);
            mTextTowDestinationAddressStreet.setVisibility(View.GONE);
            mTextTowDestinationAddressCityPincode.setVisibility(View.GONE);
            mTextEditTowDestination.setVisibility(View.GONE);
            mTextNightDropOff.setVisibility(View.GONE);
            mTextStorage.setVisibility(View.GONE);
            mTextFacilityHours.setVisibility(View.GONE);
            mTextEstimatedArrivalTime.setVisibility(View.GONE);
        }

        //Vehicle Details
        Vehicle vehicle = job.getVehicle();
        mTextVehicleDetailsName.setText(vehicle.getYear() + " " + vehicle.getMake() + " " + vehicle.getModel());
        mTextVin.setText(getString(R.string.add_job_review_vin) + " " + vehicle.getVin());
        mTextTypeClass.setText(getString(R.string.add_job_review_type_class) + " " + vehicle.getVehicleType());
        mTextDrivetrain.setText(getString(R.string.add_job_review_drive_train) + " " + vehicle.getDriveTrainType());
        mTextFuelType.setText(getString(R.string.add_job_review_fuel_type) + " " + vehicle.getFuelType());

        //Customer Details
        mTextCusomterDetailsName.setText(job.getCustomerName());
        mTextCusomterDetailsContactNumber.setText(job.getCustomerCallbackNumber());
        mTextCusomterDetailsPayment.setText(getString(R.string.add_job_review_payment) + " " + job.getCustomerPays());

        //Comments
        mTextEnteredComments.setText(getString(R.string.add_job_review_entered_comments));

    }
}
