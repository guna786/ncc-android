package com.agero.ncc.fragments;


import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import com.agero.ncc.R;
import com.agero.ncc.activities.HomeActivity;
import com.agero.ncc.model.VehicleReportModelEntity;
import com.agero.ncc.utils.FileUtils;
import com.agero.ncc.utils.NccConstants;
import com.agero.ncc.utils.Utils;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class VehicleReportTaggingFragment extends BaseFragment implements HomeActivity.ToolbarSaveCancelListener {

    @BindView(R.id.image_view_vehicle_photo)
    ViewPager mDocumentPager;
    @BindView(R.id.button_photo)
    Button mTagPhoto;
    @BindView(R.id.button_video)
    Button mTagVideo;
    @BindView(R.id.button_document)
    Button mTagDocument;
    @BindView(R.id.button_receipt)
    Button mTagReceipt;
    @BindView(R.id.button_before_service)
    Button mTagBeforeService;
    @BindView(R.id.button_vehicle_loaded)
    Button mTagVehicleLoaded;
    @BindView(R.id.button_service_complete)
    Button mTagServiceComplete;
    @BindView(R.id.total_lay)
    ConstraintLayout mConstraintLayout;

    private ArrayList<Integer> mRemovedPosition = new ArrayList<>();

    private ArrayList<View> mRootView = new ArrayList<>();

    private HomeActivity mHomeActivity;
    private ArrayList<VehicleReportModelEntity> mOldVehicleReportModelEntities = new ArrayList<>();
    private ArrayList<VehicleReportModelEntity> mVehicleReportModelEntities;
    private int mSelectedDocPosition = 0;

    private DocumentViewpagerAdapter mDocumentViewpagerAdapter;
    private boolean isEditable;

    enum TextviewStatus {
        STATUS_SELECTED, STATUS_DEFAULT, STATUS_DISABLED;
    }

    public static VehicleReportTaggingFragment newInstance(String dispatchId, boolean isEditable, ArrayList<VehicleReportModelEntity> vehicleReportModelEntities, String notes, int index, ArrayList<VehicleReportModelEntity> firebaseData, boolean isItFromHistory) {
        VehicleReportTaggingFragment fragment = new VehicleReportTaggingFragment();
        Bundle bundle = new Bundle();
        bundle.putString(NccConstants.DISPATCH_REQUEST_NUMBER, dispatchId);
        bundle.putBoolean(NccConstants.IS_EDITABLE, isEditable);
        bundle.putInt(NccConstants.VEHICLE_REPORT_DOC_INDEX, index);
        bundle.putString(NccConstants.NOTES, notes);
        if (vehicleReportModelEntities != null && vehicleReportModelEntities.get(0).getDocUrl() == null && vehicleReportModelEntities.get(0).getLocalUri() == null) {
            vehicleReportModelEntities.remove(0);
        }
        bundle.putSerializable(NccConstants.VEHICLE_REPORT_ENTITIES, vehicleReportModelEntities);
        bundle.putSerializable(NccConstants.SERVER_DATA, firebaseData);
        bundle.putBoolean(NccConstants.IS_IT_FROM_HISTORY, isItFromHistory);
        fragment.setArguments(bundle);
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View superView = super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup fragment_content = superView.findViewById(R.id.fragment_content);

        View view = inflater.inflate(R.layout.fragment_report_tagging, fragment_content, true);
        ButterKnife.bind(this, view);

        mHomeActivity = (HomeActivity) getActivity();

        if (getArguments() != null) {
            isEditable = getArguments().getBoolean(NccConstants.IS_EDITABLE);
            mVehicleReportModelEntities = (ArrayList<VehicleReportModelEntity>) getArguments().getSerializable(NccConstants.VEHICLE_REPORT_ENTITIES);
            mSelectedDocPosition = getArguments().getInt(NccConstants.VEHICLE_REPORT_DOC_INDEX);
            mOldVehicleReportModelEntities.clear();
            for (int i = 0; i < mVehicleReportModelEntities.size(); i++) {
                if (mVehicleReportModelEntities.get(i).getTags() != null) {
                    mOldVehicleReportModelEntities.add(new VehicleReportModelEntity(mVehicleReportModelEntities.get(i).getType(), mVehicleReportModelEntities.get(i).getLocalUri(), mVehicleReportModelEntities.get(i).getDocUrl(), mVehicleReportModelEntities.get(i).getThumbUrl(), mVehicleReportModelEntities.get(i).getTags(), mVehicleReportModelEntities.get(i).getVideoLength()));
                }
            }
        }

        mHomeActivity.setOnToolbarSaveCancelListener(this);

        mDocumentViewpagerAdapter = new DocumentViewpagerAdapter();
        mDocumentPager.setAdapter(mDocumentViewpagerAdapter);
        mDocumentPager.setClipToPadding(false);
        int margin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 25 * 2, getResources().getDisplayMetrics());
        mDocumentPager.setPageMargin(-margin);

        mDocumentPager.setCurrentItem(mSelectedDocPosition);
        setTagForPosition(mSelectedDocPosition);
        mDocumentPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                mSelectedDocPosition = position;
                setToolbartext(mSelectedDocPosition, mVehicleReportModelEntities.size());
                setTagForPosition(mSelectedDocPosition);
                mHomeActivity.nextEnableDisable(isTagCompletedForAllDocument());
            }


            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        setToolbartext(mSelectedDocPosition, mVehicleReportModelEntities.size());

        mHomeActivity.nextEnableDisable(isTagCompletedForAllDocument());

        return superView;
    }

    private void setToolbartext(int mSelectedDocPosition, int size) {
            mHomeActivity.showToolbar("Tag (" + (mSelectedDocPosition + 1) + " of " + size + ")", getString(R.string.vehicle_report_tag_title));
    }

    private void setTagForPosition(int selectedDocPosition) {
        ArrayList<String> tags = mVehicleReportModelEntities.get(selectedDocPosition).getTags();
        if (tags == null) {
            setTagOne(TextviewStatus.STATUS_DEFAULT);
            setTagTwo(TextviewStatus.STATUS_DEFAULT);
        } else {
            if (!TextUtils.isEmpty(tags.get(0))) {
                setTagOneEnableDisable(getViewForTagOne(tags.get(0)), false);
            } else {
                setTagOne(TextviewStatus.STATUS_DEFAULT);
            }
            if (!TextUtils.isEmpty(tags.get(1))) {
                setTagTwoEnableDisable(getViewForTagTwo(tags.get(1)), false);
            } else {
                setTagTwo(TextviewStatus.STATUS_DEFAULT);
            }

        }
    }

    private View getViewForTagOne(String tagText) {
        if (tagText.equalsIgnoreCase(mTagPhoto.getText().toString().trim())) {
            return mTagPhoto;
        } else if (tagText.equalsIgnoreCase(mTagVideo.getText().toString().trim())) {
            return mTagVideo;
        } else if (tagText.equalsIgnoreCase(mTagDocument.getText().toString().trim())) {
            return mTagDocument;
        } else {
            return mTagReceipt;
        }
    }

    private View getViewForTagTwo(String tagText) {
        if (tagText.equalsIgnoreCase(mTagVehicleLoaded.getText().toString().trim())) {
            return mTagVehicleLoaded;
        } else if (tagText.equalsIgnoreCase(mTagBeforeService.getText().toString().trim())) {
            return mTagBeforeService;
        } else {
            return mTagServiceComplete;
        }
    }


    @Override
    public void onSave() {

        saveAndStart();

    }

    private int bitmapCountNeeded;

    private void saveAndStart() {


        if (mRootView != null && mRootView.size() > 0) {
            bitmapCountNeeded = mRootView.size();
            for (View view :
                    mRootView) {
                VehicleReportModelEntity vehicleReportModelEntity = (VehicleReportModelEntity) view.getTag();
                View layout = view.getRootView().findViewById(R.id.lay);
                layout.setDrawingCacheEnabled(true);
                Bitmap bitmap = layout.getDrawingCache();
                String filePath = Utils.storeImage(getActivity(), bitmap);
                vehicleReportModelEntity.setThumbUrl(filePath);
                bitmapCountNeeded--;
                startScreen();

            }

            mRootView.clear();
        }

        startScreen();
    }

    private void startScreen() {
        if (bitmapCountNeeded == 0) {
            mHomeActivity.push(VehicleReportFragment.newInstance(getArguments().getString(NccConstants.DISPATCH_REQUEST_NUMBER),
                    getArguments().getBoolean(NccConstants.IS_EDITABLE), mVehicleReportModelEntities, getArguments().getString(NccConstants.NOTES), (ArrayList<VehicleReportModelEntity>) getArguments().getSerializable(NccConstants.SERVER_DATA), getArguments().getString(NccConstants.NOTES), getArguments().getBoolean(NccConstants.IS_IT_FROM_HISTORY)));
        }
    }

    @Override
    public void onCancel() {
        mHomeActivity.push(VehicleReportFragment.newInstance(getArguments().getString(NccConstants.DISPATCH_REQUEST_NUMBER),
                getArguments().getBoolean(NccConstants.IS_EDITABLE), mOldVehicleReportModelEntities, getArguments().getString(NccConstants.NOTES), (ArrayList<VehicleReportModelEntity>) getArguments().getSerializable(NccConstants.SERVER_DATA), getArguments().getString(NccConstants.NOTES), getArguments().getBoolean(NccConstants.IS_IT_FROM_HISTORY)));
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mHomeActivity.setOnToolbarSaveCancelListener(null);
    }


    @OnClick({R.id.button_photo, R.id.button_video, R.id.button_document, R.id.button_receipt, R.id.button_before_service, R.id.button_vehicle_loaded, R.id.button_service_complete})
    public void OnClick(View view) {
        if (isEditable) {
            switch (view.getId()) {
                case R.id.button_photo:
                case R.id.button_video:
                case R.id.button_document:
                case R.id.button_receipt:
                    setTagOneEnableDisable(view, view.isSelected());
                    mHomeActivity.nextEnableDisable(isTagCompletedForAllDocument());
                    break;
                case R.id.button_before_service:
                case R.id.button_vehicle_loaded:
                case R.id.button_service_complete:
                    setTagTwoEnableDisable(view, view.isSelected());
                    mHomeActivity.nextEnableDisable(isTagCompletedForAllDocument());
                    break;
            }
        }
    }

    private boolean isTagCompletedForAllDocument() {
        boolean isCompleted = true;
        if (mVehicleReportModelEntities != null && mVehicleReportModelEntities.size() > 0) {
            for (VehicleReportModelEntity vehicleReportModelEntity :
                    mVehicleReportModelEntities) {
                if (vehicleReportModelEntity.getTags() == null || TextUtils.isEmpty(vehicleReportModelEntity.getTags().get(0)) || TextUtils.isEmpty(vehicleReportModelEntity.getTags().get(1))) {
                    isCompleted = false;
                }
            }
        }
        return isCompleted;
    }

    private void setTagTwoEnableDisable(View view, boolean isSelected) {

        TextView selectedTv = (TextView) view;
        if (isSelected) {
            setTagTwo(TextviewStatus.STATUS_DEFAULT);
            updateTag("", 1);
        } else {
            setTagTwo(TextviewStatus.STATUS_DISABLED);

            setTextViewStatus(selectedTv, TextviewStatus.STATUS_SELECTED);

            updateTag(selectedTv.getText().toString(), 1);
        }
    }

    private void setTagOneEnableDisable(View view, boolean isSelected) {

        TextView selectedTv = (TextView) view;
        if (isSelected) {
            setTagOne(TextviewStatus.STATUS_DEFAULT);

            updateTag("", 0);
        } else {
            setTagOne(TextviewStatus.STATUS_DISABLED);

            setTextViewStatus(selectedTv, TextviewStatus.STATUS_SELECTED);

            updateTag(selectedTv.getText().toString(), 0);
        }
    }

    private void updateTag(String tagText, int i) {
        ArrayList<String> tags = mVehicleReportModelEntities.get(mSelectedDocPosition).getTags();
        if (tags == null) {
            tags = new ArrayList<>();
            tags.add("");
            tags.add("");
        }
        tags.set(i, tagText);
        mVehicleReportModelEntities.get(mSelectedDocPosition).setTags(tags);
    }

    private void setTagOne(TextviewStatus textviewStatus) {

        setTextViewStatus(mTagPhoto, textviewStatus);
        setTextViewStatus(mTagVideo, textviewStatus);
        setTextViewStatus(mTagDocument, textviewStatus);
        setTextViewStatus(mTagReceipt, textviewStatus);

    }

    private void setTagTwo(TextviewStatus textviewStatus) {

        setTextViewStatus(mTagVehicleLoaded, textviewStatus);
        setTextViewStatus(mTagBeforeService, textviewStatus);
        setTextViewStatus(mTagServiceComplete, textviewStatus);
    }


    private void setTextViewStatus(TextView textView, TextviewStatus textviewStatus) {
        switch (textviewStatus) {
            case STATUS_DEFAULT:
                textView.setSelected(false);
                textView.setEnabled(true);
                textView.setAlpha(1f);
                textView.setTextColor(getResources().getColor(R.color.ncc_blue));
                break;
            case STATUS_DISABLED:
                textView.setSelected(false);
                textView.setEnabled(false);
                textView.setAlpha(0.5f);
                textView.setTextColor(getResources().getColor(R.color.ncc_black));
                break;
            case STATUS_SELECTED:
                textView.setSelected(true);
                textView.setEnabled(true);
                textView.setAlpha(1f);
                textView.setTextColor(getResources().getColor(R.color.ncc_white));
                break;
        }
    }

    class DocumentViewpagerAdapter extends PagerAdapter {

        @BindView(R.id.image_view_vehicle_photo)
        ImageView mImageViewVehiclePhoto;
        @BindView(R.id.video_view_vehicle_photo)
        VideoView mVideoViewVehicleVideo;

        @BindView(R.id.image_video)
        ImageView mImageVideoIcon;
        @BindView(R.id.image_cancel)
        ImageView mImageCancel;
        @BindView(R.id.text_duration)
        TextView mTextVideoDuration;

        @Override
        public Object instantiateItem(ViewGroup collection, int position) {
            LayoutInflater inflater = LayoutInflater.from(mHomeActivity);
            ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.fragment_view_tagging_child, collection, false);
            ButterKnife.bind(this, layout);

            initItem(mVehicleReportModelEntities.get(position));
            if (!isEditable) {
                mImageCancel.setVisibility(View.INVISIBLE);
            } else {
                mImageCancel.setVisibility(View.VISIBLE);
            }
            mImageCancel.setTag(position);
            mImageCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = (int) view.getTag();
                    removeItem(position);
                }
            });

            collection.addView(layout);
            return layout;
        }

        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        private void initItem(VehicleReportModelEntity vehicleReportModelEntity) {

            if (vehicleReportModelEntity.getLocalUri() != null) {
                String uri = vehicleReportModelEntity.getLocalUri().getPath();
                mTextVideoDuration.setVisibility(View.GONE);
                mImageVideoIcon.setVisibility(View.GONE);
                if (FileUtils.getFileType(uri) == FileUtils.FileType.PDF) {
                    Glide.with(mHomeActivity)
                            .load(Utils.generateImageFromPdf(mHomeActivity,
                                    vehicleReportModelEntity.getLocalUri()))
                            .into(mImageViewVehiclePhoto);
                    vehicleReportModelEntity.setType("pdf");
                } else if (FileUtils.getFileType(uri) == FileUtils.FileType.VIDEO) {
                    Glide.with(mHomeActivity).load(vehicleReportModelEntity.getLocalUri()).into(mImageViewVehiclePhoto);
                    mImageViewVehiclePhoto.setTag(vehicleReportModelEntity);
                    mRootView.add(mImageViewVehiclePhoto);
                    String selectedVideoFilePath = FileUtils.getPath(mHomeActivity, vehicleReportModelEntity.getLocalUri());
                    if (selectedVideoFilePath != null) {
                        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
                        retriever.setDataSource(selectedVideoFilePath); // Enter Full File Path Here
                        String time = retriever.extractMetadata(
                                MediaMetadataRetriever.METADATA_KEY_DURATION);
                        long timeInmillisec = Long.parseLong(time);
                        double minutes = (timeInmillisec / (1000.0 * 60)) % 60;

                        mTextVideoDuration.setText(
                                String.format(Locale.getDefault(), "%.2f", minutes));
                        mTextVideoDuration.setVisibility(View.VISIBLE);
                        mImageVideoIcon.setVisibility(View.VISIBLE);
                        vehicleReportModelEntity.setType("video");
                        vehicleReportModelEntity.setVideoLength(timeInmillisec);
                    }

                } else {
                    vehicleReportModelEntity.setType("image");
                    Glide.with(mHomeActivity)
                            .load(vehicleReportModelEntity.getLocalUri())
                            .into(mImageViewVehiclePhoto);


                }
            } else {
                mTextVideoDuration.setVisibility(View.GONE);
                mImageVideoIcon.setVisibility(View.GONE);
                if (vehicleReportModelEntity.getType() != null && vehicleReportModelEntity.getType().equalsIgnoreCase("pdf")) {
                    Glide.with(mHomeActivity)
                            .load(vehicleReportModelEntity.getThumbUrl())
                            .into(mImageViewVehiclePhoto);

                } else if (vehicleReportModelEntity.getType() != null && vehicleReportModelEntity.getType().equalsIgnoreCase("video")) {
                    mTextVideoDuration.setVisibility(View.VISIBLE);
                    mImageVideoIcon.setVisibility(View.VISIBLE);

                    if (vehicleReportModelEntity.getVideoLength() != null) {
                        double minutes = (vehicleReportModelEntity.getVideoLength() / (1000.0 * 60)) % 60;
                        mTextVideoDuration.setText(
                                String.format(Locale.getDefault(), "%.2f", minutes));
                    }
                    Glide.with(mHomeActivity)
                            .load(vehicleReportModelEntity.getThumbUrl())
                            .into(mImageViewVehiclePhoto);

                } else {
                    Glide.with(mHomeActivity)
                            .load(vehicleReportModelEntity.getDocUrl())
                            .into(mImageViewVehiclePhoto);
                }
            }
        }

        private void playVideo(String path) {
            showProgress();
            mImageViewVehiclePhoto.setVisibility(View.GONE);
            mVideoViewVehicleVideo.setVisibility(View.VISIBLE);
            MediaController mc = new MediaController(getActivity());

            mVideoViewVehicleVideo.setVideoURI(Uri.parse(path));
            mVideoViewVehicleVideo.setMediaController(mc);
            mVideoViewVehicleVideo.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mediaPlayer) {
                    hideProgress();
                }
            });
            mVideoViewVehicleVideo.start();
        }

        @Override
        public void destroyItem(ViewGroup collection, int position, Object view) {
            collection.removeView((View) view);
        }

        @Override
        public int getCount() {

            return mVehicleReportModelEntities.size(); //- mRemovedPosition.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }
    }

    private void removeItem(int position) {
        mVehicleReportModelEntities.remove(position);
        mRemovedPosition.add(position);
        if (mVehicleReportModelEntities.size() == 0) {
            saveAndStart();
        } else {
            if (position >= mVehicleReportModelEntities.size()) {
                position = position - 1;
            }
            mSelectedDocPosition = position;
            mDocumentViewpagerAdapter.notifyDataSetChanged();
            mDocumentPager.setCurrentItem(position);
            setTagForPosition(mSelectedDocPosition);
        }
        setToolbartext(mSelectedDocPosition, mVehicleReportModelEntities.size());
        mHomeActivity.nextEnableDisable(isTagCompletedForAllDocument());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mHomeActivity.setOnToolbarSaveCancelListener(null);
    }
}

