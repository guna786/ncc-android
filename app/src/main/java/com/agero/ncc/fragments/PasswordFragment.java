package com.agero.ncc.fragments;


import android.Manifest;
import android.app.KeyguardManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.GradientDrawable;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import okhttp3.ResponseBody;

import com.agero.ncc.BuildConfig;
import com.agero.ncc.R;
import com.agero.ncc.activities.BaseActivity;
import com.agero.ncc.activities.HomeActivity;
import com.agero.ncc.activities.WelcomeActivity;
import com.agero.ncc.app.NCCApplication;
import com.agero.ncc.db.database.NccDatabase;
import com.agero.ncc.model.AuthTokenModel;
import com.agero.ncc.model.ForgotPasswordRequest;
import com.agero.ncc.model.SigninRequest;
import com.agero.ncc.model.SigninResponseModel;
import com.agero.ncc.retrofit.NccApi;
import com.agero.ncc.services.SparkLocationUpdateService;
import com.agero.ncc.utils.DeCryptor;
import com.agero.ncc.utils.EnCryptor;
import com.agero.ncc.utils.NccConstants;
import com.agero.ncc.utils.TokenManager;
import com.agero.ncc.utils.UiUtils;
import com.agero.ncc.utils.UserError;
import com.agero.ncc.utils.Utils;
import com.github.ajalt.reprint.core.AuthenticationFailureReason;
import com.github.ajalt.reprint.core.AuthenticationListener;
import com.github.ajalt.reprint.core.Reprint;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SignatureException;
import java.security.UnrecoverableEntryException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.inject.Inject;
import javax.security.cert.CertificateException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static android.content.Context.FINGERPRINT_SERVICE;
import static android.content.Context.KEYGUARD_SERVICE;
import static com.agero.ncc.utils.NccConstants.IS_FINGERPRINT_ENABLED;
import static com.agero.nccsdk.NccSdk.getApplicationContext;


public class PasswordFragment extends BaseFragment {


    @Inject
    NccDatabase nccDatabase;
    WelcomeActivity mWelcomeActivity;
    @BindView(R.id.edit_email)
    EditText mEditEmail;
    @BindView(R.id.text_input_email)
    TextInputLayout mTextInputEmail;
    @BindView(R.id.edit_password)
    EditText mEditPassword;
    @BindView(R.id.text_input_password)
    TextInputLayout mTextInputPassword;
    @BindView(R.id.button_password_continue)
    Button mButtonPasswordContinue;
    @BindView(R.id.image_password_back_arrow)
    ImageView mImagePasswordBackArrow;
    UserError mUserError;
    String invitationCode;
    @BindView(R.id.coordinate_edit_profile)
    CoordinatorLayout mCoordinateEditProfile;
    String eventAction = "";
    String eventCategory = NccConstants.FirebaseEventCategory.SIGN_IN;

    private HashMap<String, Integer> mForgotPasswordCounter = new HashMap<>();


    @Inject
    NccApi nccApi;

    String emailAddress;
    ForgotPasswordDialogFragment forgotPasswordDialogFragment;
    FingerprintAuthenticationDialogFragment fragment = new FingerprintAuthenticationDialogFragment();
    static boolean isEmailSent;

    public PasswordFragment() {
        // Intentionally empty
    }

    public static PasswordFragment newInstance(String emailAddress) {
        PasswordFragment fragment = new PasswordFragment();
        Bundle args = new Bundle();
        args.putString("emailAddress", emailAddress);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View superView = super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup fragment_content = superView.findViewById(R.id.fragment_content);
        View view = inflater.inflate(R.layout.fragment_password, fragment_content, true);
        NCCApplication.getContext().getComponent().inject(this);
        ButterKnife.bind(this, view);
        TokenManager.getInstance().signOut();
        mEditEmail.addTextChangedListener(mTextWatcher);
        mEditPassword.addTextChangedListener(mTextWatcher);
        mWelcomeActivity = (WelcomeActivity) getActivity();
        mUserError = new UserError();
        Toolbar toolbar = (Toolbar) mWelcomeActivity.findViewById(R.id.toolbar);
        toolbar.setVisibility(View.GONE);
        mButtonPasswordContinue.setEnabled(false);
        mButtonPasswordContinue.setBackgroundResource(R.drawable.border_corner_history);
        if (!mPrefs.getString(NccConstants.SIGNIN_EMAILADDRESS, "").isEmpty()) {
            mEditEmail.setText(mPrefs.getString(NccConstants.SIGNIN_EMAILADDRESS, ""));
        }
        GradientDrawable drawableCorner = (GradientDrawable) mButtonPasswordContinue.getBackground();
        drawableCorner.setColor(getResources().getColor(R.color.createacc_button_disable_color));

        mEditEmail.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                mTextInputEmail.setEnabled(true);
                mTextInputEmail.setErrorEnabled(false);
            }
        });
        mEditPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                validEmail();
                if (b) {
                    mTextInputPassword.setEnabled(true);
                    mTextInputPassword.setErrorEnabled(false);
                } else {
                    if (mEditPassword.getText().toString().isEmpty()) {
                        mTextInputPassword.setEnabled(true);
                        mTextInputPassword.setErrorEnabled(true);
                        mTextInputPassword.setError(getString(R.string.password_hint_error));
                    }
                }

               /* if (!mEditPassword.getText().toString().isEmpty()) {
                    mTextInputPassword.setErrorEnabled(false);
                } else {
                    mTextInputPassword.setErrorEnabled(true);
                    mTextInputPassword.setError(getString(R.string.password_hint_error));
                }*/
            }
        });
        mEditPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    proceedSignin();
                }
                return false;
            }
        });

        return superView;

    }

    @Override
    public void onResume() {
        super.onResume();
        if (IS_FINGERPRINT_ENABLED) {
            if (TextUtils.isEmpty(mEditPassword.getText().toString()) &&
                    !mPrefs.getString(NccConstants.SIGNIN_PASSWORD, "").isEmpty() &&
                    canRequestFingerPrintAuthentication()) {
                if(fragment != null && fragment.isAdded()){
                    try {
                        fragment.dismiss();
                        Reprint.cancelAuthentication();
                    }catch (Exception e){

                    }
                }
                fragment.setFragmentCallbackLister(new FingerprintAuthenticationDialogFragment.FingerPrintCallBackListener() {
                    @Override
                    public void onSuccess() {
                        if (isAdded()) {
                            onFingerPrintSuccess();
                        }
                    }

                    @Override
                    public void onFailure() {

                    }
                });
                fragment.show(mWelcomeActivity.getFragmentManager(), "FingerprintAuthenticationDialogFragment");
            }
        }
    }

    private void proceedSignin() {
        mButtonPasswordContinue.setClickable(false);
        String email = mEditEmail.getText().toString().trim();


        if (Utils.isNetworkAvailable()) {
            UiUtils.hideKeyboard(mButtonPasswordContinue);
            signin();
        } else {
            mUserError.title = "";
            mUserError.message = getString(R.string.network_error_message);
            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
            mButtonPasswordContinue.setClickable(true);

        }
    }

    /*@Override
    public void onDestroyView() {
        super.onDestroyView();
        String json = new Gson().toJson(mForgotPasswordCounter);
        mPrefs.edit().putString(NccConstants.FORGOT_PWD_COUNTER,json).apply();
    }*/

    @OnClick({R.id.text_forgot_password, R.id.button_password_continue, R.id.image_password_back_arrow})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.text_forgot_password:
                startForgotPassword(mEditEmail.getText().toString());
                break;
            case R.id.button_password_continue:
                proceedSignin();
                eventAction = NccConstants.FirebaseEventAction.CONTINUE;
                mWelcomeActivity.firebaseLogEvent(eventCategory, eventAction);
                break;
            case R.id.image_password_back_arrow:
                getActivity().onBackPressed();
                break;
        }
    }

    private void startForgotPassword(String emailAddress) {
        forgotPasswordDialogFragment = ForgotPasswordDialogFragment.newInstance(emailAddress);
        forgotPasswordDialogFragment.show(getActivity().getFragmentManager(), "");
        forgotPasswordDialogFragment.setOnResetClick(new ForgotPasswordDialogFragment.OnResetClick() {
            @Override
            public void onResetClicked(String mailId) {
                resetPassword(mailId);
                eventAction = NccConstants.FirebaseEventAction.RESET_PASSWORD;
                mWelcomeActivity.firebaseLogEvent(eventCategory, eventAction);
            }
        });
        eventAction = NccConstants.FirebaseEventAction.FORGOT_PASSWORD;
        mWelcomeActivity.firebaseLogEvent(eventCategory, eventAction);
    }

    private void resetPassword(String mailId) {

        if (Utils.isNetworkAvailable()) {
            showProgress();

            ForgotPasswordRequest request = new ForgotPasswordRequest(mailId, TokenManager.getInstance().getDeviceId());

            HashMap<String, String> extraDatas = new HashMap<>();
            extraDatas.put("json", new Gson().toJson(request));
            mWelcomeActivity.mintlogEventExtraData("Reset Password Request", extraDatas);

            nccApi.forgotPassword(BuildConfig.APIGEE_URL + "/profile/request-password-reset-code", BuildConfig.APIGEE_API_KEY, request).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                    if (isAdded()) {
                        if (response != null && response.code() == 200) {
                            hideProgress();
                            Snackbar.make(mCoordinateEditProfile, getString(R.string.password_reset_sent), Snackbar.LENGTH_SHORT).show();
                            mForgotPasswordCounter.clear();
                            saveCounter();
                        } else if (response.errorBody() != null) {
                            hideProgress();
                            try {
                                JSONObject errorResponse = new JSONObject(response.errorBody().string());
                                if (errorResponse != null) {
                                    if (errorResponse.get("message") != null && errorResponse.get("message").toString().length() > 1 && isAdded()) {
                                        invalidPasswordAlert((String) errorResponse.get("message"));
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    hideProgress();
                    if (isAdded()) {
                        mWelcomeActivity.mintlogEvent("Password Screen Reset Password Api Error - " + t.getLocalizedMessage());
                        if (mWelcomeActivity != null && !mWelcomeActivity.isFinishing()) {
                            Toast.makeText(mWelcomeActivity, getString(R.string.auth_failed), Toast.LENGTH_LONG).show();
                        }
                    }
                }
            });
        } else {
            mUserError.title = "";
            mUserError.message = getString(R.string.network_error_message);
            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
            mButtonPasswordContinue.setClickable(true);

        }
    }


    private void signin() {
        showProgress();
        String password = mEditPassword.getText().toString();
        String emailaddress = mEditEmail.getText().toString();

        SigninRequest request = new SigninRequest(emailaddress, password, TokenManager.getInstance().getDeviceId());

        HashMap<String, String> extraDatas = new HashMap<>();
        extraDatas.put("json", new Gson().toJson(request));
        mWelcomeActivity.mintlogEventExtraData("Sign In Request", extraDatas);

        nccApi.Signin(BuildConfig.APIGEE_URL + "/signin", BuildConfig.APIGEE_API_KEY, request).enqueue(new Callback<SigninResponseModel>() {
            @Override
            public void onResponse(Call<SigninResponseModel> call, Response<SigninResponseModel> response) {

                if (isAdded()) {
                    if (response != null && response.body() != null) {
                        Timber.d(response.body().toString());
                        SigninResponseModel responseModel = response.body();

                        if (mPrefs.getString(NccConstants.SIGNIN_EMAILADDRESS, "") != null && !TextUtils.isEmpty(mEditEmail.getText())) {

                            if (!mPrefs.getString(NccConstants.SIGNIN_EMAILADDRESS, "").equalsIgnoreCase(mEditEmail.getText().toString().trim())) {
                                nccDatabase.getAlertDao().deleteAll();
                            }
                        }

                        mEditor.putString(NccConstants.SIGNIN_EMAILADDRESS, mEditEmail.getText().toString()).commit();

                        mEditor.putString(NccConstants.SIGNIN_VENDOR_ID, responseModel.getUser_claims().getFacility_id()).commit();
                        mEditor.putString(NccConstants.SIGNIN_USER_ID, responseModel.getUser_claims().getUser_id()).commit();
                        mEditor.putString(NccConstants.SIGNIN_USER_NAME, responseModel.getUser_profile().getFirstName() + " " + responseModel.getUser_profile().getLastName()).commit();
                        String roles = new Gson().toJson(responseModel.getUser_claims().getRoles());
//                        ArrayList<String> rolesArray = new ArrayList<>();
//                        rolesArray.add(NccConstants.USER_ROLE_DRIVER);
//                        String roles = new Gson().toJson(rolesArray);
                        mEditor.putString(NccConstants.SIGNIN_USER_ROLE, roles).commit();
                        mEditor.putString(NccConstants.APIGEE_TOKEN, responseModel.getId_token()).commit();
                        mEditor.putString(NccConstants.FIREBASE_TOKEN, responseModel.getFirebase_token()).commit();
                        long expiresIn = (System.currentTimeMillis()) + (responseModel.getExpires_in() * 1000);
                        expiresIn = expiresIn - 600000;

                        SimpleDateFormat outputFmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.getDefault());


                        Date expireDate = new Date(expiresIn);

                        Timber.d("expiryTime: " + outputFmt.format(expireDate));

                        Timber.d("currentTimeM: " + outputFmt.format(new Date(System.currentTimeMillis())));

                        mEditor.putLong(NccConstants.APIGEE_TOKEN_EXPIRE, expiresIn).commit();
                        mEditor.putString(NccConstants.FIREBASE_REFRESH_TOKEN, responseModel.getRefresh_token()).commit();
//                            mWelcomeActivity.mintlogEvent("User - "+responseModel.getUser_claims().getUser_id() + " Sign in");

//                            String eventJson = new Gson().toJson(responseModel);

//                            SigninResponseModel signinResponseModel = new Gson().fromJson(eventJson, SigninResponseModel.class);
//
//                            signinResponseModel.setRefresh_token(null);
//                            signinResponseModel.setId_token(null);
//                            signinResponseModel.setFirebase_token(null);

                        HashMap<String, String> extraDatas = new HashMap<>();
                        extraDatas.put("json", new Gson().toJson(responseModel));
                        mWelcomeActivity.mintlogEventExtraData("Sign In Response", extraDatas);

                        mForgotPasswordCounter.clear();
                        saveCounter();

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && IS_FINGERPRINT_ENABLED) {
                            EnCryptor encryptor = new EnCryptor();
                            try {
                                encryptor.encryptText("password", password);
                                mEditor.putString(NccConstants.SIGNIN_PASSWORD, Base64.encodeToString(encryptor.getEncryption(), Base64.DEFAULT)).commit();
                                mEditor.putString(NccConstants.SIGNIN_PASSWORD_IV, Base64.encodeToString(encryptor.getIv(), Base64.DEFAULT)).commit();
                            } catch (UnrecoverableEntryException | NoSuchAlgorithmException | NoSuchProviderException |
                                    KeyStoreException | IOException | NoSuchPaddingException | InvalidKeyException e) {
                                e.printStackTrace();
                                ///Log.e(TAG, "onClick() called with: " + e.getMessage(), e);
                            } catch (InvalidAlgorithmParameterException | SignatureException |
                                    IllegalBlockSizeException | BadPaddingException e) {
                                e.printStackTrace();
                            }
                        }


                        TokenManager.getInstance().signinFirebase(mWelcomeActivity, responseModel.getFirebase_token(), expiresIn, new TokenManager.TokenReponseListener() {
                            @Override
                            public void onRefreshSuccess() {
                                hideProgress();
                                if (isAdded()) {
                                    startHomeScreen();
                                }
                            }

                            @Override
                            public void onRefreshFailure() {
                                hideProgress();
                                if (isAdded()) {
                                    mButtonPasswordContinue.setClickable(true);
                                    Toast.makeText(mWelcomeActivity, getString(R.string.auth_failed), Toast.LENGTH_LONG).show();
                                }
                            }
                        }, responseModel.getId_token());

                    } else if (response.errorBody() != null) {
                        mButtonPasswordContinue.setClickable(true);
                        hideProgress();
                        try {
                            JSONObject errorResponse = new JSONObject(response.errorBody().string());
                            if (errorResponse != null) {

                                int counter = 0;

                                if (mPrefs.contains(NccConstants.FORGOT_PWD_COUNTER)) {

                                    String forgotPwdCounter = mPrefs.getString(NccConstants.FORGOT_PWD_COUNTER, "");
                                    if (!TextUtils.isEmpty(forgotPwdCounter)) {
                                        Type type = new TypeToken<HashMap<String, Integer>>() {
                                        }.getType();
                                        mForgotPasswordCounter = new Gson().fromJson(forgotPwdCounter, type);
                                    }
                                }
                                if (mForgotPasswordCounter.containsKey(emailaddress)) {
                                    counter = mForgotPasswordCounter.get(emailaddress);
                                }
                                if (response.code() == 403) {
                                    if (counter < 3) {
                                        if (errorResponse.get("message") != null && errorResponse.get("message").toString().length() > 1 && isAdded()) {
                                            increaseCount(mEditEmail.getText().toString().trim());
                                            invalidPasswordAlert((String) errorResponse.get("message"));
                                        }
                                    } else {
                                        tooManyAttemptsPasswordAlert(emailaddress);
                                    }
                                } else {
                                    if (errorResponse.get("message") != null && errorResponse.get("message").toString().length() > 1 && isAdded()) {
                                        invalidPasswordAlert((String) errorResponse.get("message"));
                                    }
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<SigninResponseModel> call, Throwable t) {
                hideProgress();
                if (isAdded()) {
                    mButtonPasswordContinue.setClickable(true);
                    mWelcomeActivity.mintlogEvent("Password Screen Signin Api Error - " + t.getLocalizedMessage());
                    Toast.makeText(mWelcomeActivity, getString(R.string.auth_failed), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void increaseCount(String emailaddress) {
        if (mForgotPasswordCounter.containsKey(emailaddress)) {
            int counter = mForgotPasswordCounter.get(emailaddress) + 1;
            mForgotPasswordCounter.put(emailaddress, counter);
        } else {
            mForgotPasswordCounter.put(emailaddress, 1);
        }
        saveCounter();
    }

    private void saveCounter() {
        String json = new Gson().toJson(mForgotPasswordCounter);
        mPrefs.edit().putString(NccConstants.FORGOT_PWD_COUNTER, json).apply();
    }


    private void startHomeScreen() {
        Intent intent = new Intent(getActivity(), HomeActivity.class);
        SparkLocationUpdateService.clearList();
        startActivity(intent);
        mWelcomeActivity.finish();
    }

    private void invalidPasswordAlert(String message) {
        AlertDialogFragment alert = AlertDialogFragment.newDialog("", Html.fromHtml("<big>" + message + "</big>")
                , Html.fromHtml("<b>" + getString(R.string.password_alert_hint_tryagain) + "</b>"), "");

        alert.setListener(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                dialogInterface.dismiss();
            }
        });
        alert.show(getFragmentManager(), PasswordFragment.class.getClass().getCanonicalName());
    }

    private void tooManyAttemptsPasswordAlert(String emailAddress) {
        AlertDialogFragment alert = AlertDialogFragment.newDialog(Html.fromHtml("<big>" + getString(R.string.password_alert_too_many_title) + "</big>")
                , Html.fromHtml("<font color=\"#666666\">" + getString(R.string.password_alert_too_many_description) + "</font>"), "Cancel", Html.fromHtml(getString(R.string.password_alert_too_many_reset_password)));

        alert.setListener(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (isAdded()) {
                    if (i == -2 && !mWelcomeActivity.isFinishing()) {
                        startForgotPassword(emailAddress);
                    }
                    mButtonPasswordContinue.setClickable(true);
                    dialogInterface.dismiss();
                }
            }


        });
        alert.show(getFragmentManager().beginTransaction(), PasswordFragment.class.getClass().getCanonicalName());
    }

    private void showOnlyDriverDialog() {
        AlertDialogFragment alert = AlertDialogFragment.newDialog(getString(R.string.only_driver_dialog_title)
                , Html.fromHtml("<font color=\"#666666\">" + getString(R.string.only_driver_dialog_content) + "</font>"), "", getString(R.string.dismiss_text));
        alert.show(getFragmentManager().beginTransaction(), PasswordFragment.class.getClass().getCanonicalName());
    }

    private void showSelectRoleDialog() {
        AlertDialogFragment alert = AlertDialogFragment.newDialog("Choose Role"
                , "Login as Driver or Dispatcher", "Dispatcher", "Driver");

        alert.setListener(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                ArrayList<String> roleList = new ArrayList<>();
                if (i == DialogInterface.BUTTON_POSITIVE) {
                    roleList.add(NccConstants.USER_ROLE_DISPATCHER);
                } else if (i == DialogInterface.BUTTON_NEGATIVE) {
                    roleList.add(NccConstants.USER_ROLE_DRIVER);
                }
                mEditor.putString(NccConstants.SIGNIN_USER_ROLE, new Gson().toJson(roleList)).commit();
                if (isAdded() && getActivity() != null && !getActivity().isFinishing()) {
                    dialogInterface.dismiss();
                    ((WelcomeActivity) getActivity()).pushFragment(PermissionFragment.newInstance());
                }
            }
        });
        alert.show(getFragmentManager().beginTransaction(), PasswordFragment.class.getClass().getCanonicalName());
    }


    private final TextWatcher mTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable s) {
//            if (mEditEmail.isFocused()) {
//                if (!mEditPassword.getText().toString().isEmpty()) {
//                    mTextInputPassword.setErrorEnabled(false);
//                }
//            }
//            if (mEditPassword.isFocused()) {
            enableContinueButton();
//            }

        }
    };

    private void enableContinueButton() {
        GradientDrawable drawbleCorner = (GradientDrawable) mButtonPasswordContinue.getBackground();
        if (isValidEmail(mEditEmail.getText().toString())
                && !mEditPassword.getText().toString().isEmpty()) {
            mButtonPasswordContinue.setEnabled(true);
            drawbleCorner.setColor(getResources().getColor(R.color.ncc_background_button));
            mButtonPasswordContinue.setTextColor(getResources().getColor(R.color.ncc_white));
        } else {
            mButtonPasswordContinue.setEnabled(false);
            drawbleCorner.setColor(getResources().getColor(R.color.createacc_button_disable_color));
            mButtonPasswordContinue.setTextColor(getResources().getColor(R.color.partnercode_button_text_color));
        }
    }

    private void validEmail() {
        if (isValidEmail(mEditEmail.getText().toString().trim())) {
            mTextInputEmail.setEnabled(true);
            mTextInputEmail.setErrorEnabled(false);
        } else {
            mTextInputEmail.setErrorEnabled(true);
            if (!mEditEmail.getText().toString().trim().isEmpty()) {
                mTextInputEmail.setError(getResources().getString(R.string.createacc_error_email_format));
            } else {
                mTextInputEmail.setError(getResources().getString(R.string.createacc_error_email));
            }
        }
    }

    private boolean canRequestFingerPrintAuthentication() {
        // If you’ve set your app’s minSdkVersion to anything lower than 23, then you’ll need to verify that the device is running Marshmallow
        // or higher before executing any fingerprint-related code
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            //Get an instance of KeyguardManager and FingerprintManager//
            KeyguardManager keyguardManager = (KeyguardManager) mWelcomeActivity.getSystemService(KEYGUARD_SERVICE);
            FingerprintManager fingerprintManager = (FingerprintManager) mWelcomeActivity.getSystemService(FINGERPRINT_SERVICE);

            //Check whether the device has a fingerprint sensor//
            //Check whether the user has granted your app the USE_FINGERPRINT permission//
            //Check that the user has registered at least one fingerprint//
            //Check that the lockscreen is secured//
            return fingerprintManager != null && fingerprintManager.isHardwareDetected() &&
                    ActivityCompat.checkSelfPermission(mWelcomeActivity, Manifest.permission.USE_FINGERPRINT) == PackageManager.PERMISSION_GRANTED &&
                    fingerprintManager.hasEnrolledFingerprints() &&
                    keyguardManager.isKeyguardSecure();

        }
        return false;
    }

    public void onFingerPrintSuccess() {
        //showSuccess();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            DeCryptor decryptor = null;
            try {
                decryptor = new DeCryptor();
            } catch (java.security.cert.CertificateException | NoSuchAlgorithmException | KeyStoreException |
                    IOException e) {
                e.printStackTrace();
            }

            try {
                String encryPassword = mPrefs.getString(NccConstants.SIGNIN_PASSWORD, null);
                String encryPasswordIv = mPrefs.getString(NccConstants.SIGNIN_PASSWORD_IV, null);
                String password = decryptor.decryptData("password", Base64.decode(encryPassword, 0), Base64.decode(encryPasswordIv, 0));
                mEditPassword.setText(password);
                mButtonPasswordContinue.performClick();
            } catch (UnrecoverableEntryException | NoSuchAlgorithmException |
                    KeyStoreException | NoSuchPaddingException | NoSuchProviderException |
                    IOException | InvalidKeyException e) {
                //Log.e(TAG, "decryptData() called with: " + e.getMessage(), e);
            } catch (IllegalBlockSizeException | BadPaddingException | InvalidAlgorithmParameterException e) {
                e.printStackTrace();
            }
        }
    }
}
