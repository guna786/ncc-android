# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /Users/sdevabhaktuni/Library/Android/sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile
-dontwarn android.support.**
-dontwarn retrofit2.**
-dontwarn io.**
-dontwarn javax.**
-dontwarn com.splunk.**
-dontwarn org.**
-dontwarn com.agero.**
-dontwarn me.**
-dontwarn com.auth0.**
-dontwarn com.amazonaws.**
-keep class android.support.** { *; }
-keep class retrofit2.** { *; }
-keep class io.** { *; }
-keep class javax.** { *; }
-keep class com.splunk.** { *; }
-keep class org.** { *; }
-keep class com.agero.** { *; }
-keep class me.** { *; }
-keep class com.auth0.** { *; }
-keep class com.amazonaws.** {*;}
-keep class co.chatsdk.core.** {*;}